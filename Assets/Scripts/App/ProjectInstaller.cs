﻿using DungeonLiege.Core;
using Zenject;

namespace DungeonLiege.App {
    public class ProjectInstaller : MonoInstaller
    {
        public ProjectInstaller()
        {
        }

        public override void InstallBindings(){
            Container.Bind<ILevelLoader>().To<LevelLoader>().AsSingle();
            Container.Bind<IFileIO>().To<FileIO>().AsSingle();
            Container.Bind<IDirectoryIO>().To<DirectoryIO>().AsSingle();
            Container.Bind<IJsonIO>().To<JsonIO>().AsSingle();
            Container.Bind<IBoardManager>().To<BoardManager>().AsSingle();
            Container.Bind<ICampaignManager>().To<CampaignManager>().AsSingle();
            Container.Bind<ICampaignMetaManager>().To<CampaignMetaManager>().AsSingle();
            Container.Bind<ISceneData>().To<SceneData>().AsSingle();
        }
    }
}
