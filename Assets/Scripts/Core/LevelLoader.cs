﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DungeonLiege.Core
{
    public class LevelLoader : ILevelLoader
    {
        public void LoadLevel(int level)
        {
            SceneManager.LoadScene(level);
        }
    }
}

