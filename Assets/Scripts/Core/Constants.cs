﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonLiege.Core
{

    public class Constants
    {
        public static string CAMPAIGN_META_FILE_NAME = "meta.json";
        public static string CAMPAIGN_FILE_NAME = "campaign.json";

        public static string ENTITY_CATEGORY_FLOORS = "Floors";
        public static string ENTITY_CATEGORY_WALLS = "Walls, Windows, and Doors";
        public static string ENTITY_CATEGORY_PROPS = "Props, and Decorations";

        public enum Levels
        {
            TitleMenu = 0,
            Editor = 1,
            CampaignMenu = 2,
            Game = 3
        }
    }
}
