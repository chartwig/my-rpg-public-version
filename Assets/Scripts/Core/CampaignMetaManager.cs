﻿using System.IO;
using Zenject;
using DungeonLiege.Entities;
using System.Collections.Generic;

namespace DungeonLiege.Core
{
    public class CampaignMetaManager : ICampaignMetaManager
    {
        private IFileIO _FileIO { get; set; }
        private IDirectoryIO _DirectoryIO { get; set; }

        [Inject]
        public void Init(IFileIO fileIO, IDirectoryIO directoryIO)
        {
            _FileIO = fileIO;
            _DirectoryIO = directoryIO;
        }

        public void Save(CampaignMeta metaData)
        {
            string path = _FileIO.GetModulePath();

            if (!_DirectoryIO.Exists(path))
            {
                _DirectoryIO.Create(path);
            }

            string modulePath = Path.Combine(path, metaData.Name);
            string metaPath;

            if (!_DirectoryIO.Exists(modulePath))
            {
                _DirectoryIO.Create(modulePath);
            }

            metaPath = Path.Combine(modulePath, Constants.CAMPAIGN_META_FILE_NAME);

            _FileIO.SaveToConfig<CampaignMeta>(metaPath, metaData);

        }

        public CampaignMeta Load(string campaignMetaName)
        {
            string path = _FileIO.GetModulePath();

            if (!_DirectoryIO.Exists(path))
            {
                _DirectoryIO.Create(path);
            }

            string modulePath = Path.Combine(path, campaignMetaName);
            string metaPath;

            if (!_DirectoryIO.Exists(modulePath))
            {
                _DirectoryIO.Create(modulePath);
            }

            metaPath = Path.Combine(modulePath, Constants.CAMPAIGN_META_FILE_NAME);

            return _FileIO.LoadFromConfig<CampaignMeta>(metaPath);
        }

        public List<CampaignMeta> LoadAll()
        {
            string path = _FileIO.GetModulePath();
            List<CampaignMeta> campaigns = new List<CampaignMeta>();

            if (!_DirectoryIO.Exists(path))
            {
                _DirectoryIO.Create(path);
            }

            foreach (string dir in _DirectoryIO.GetDirectories(path))
            {
                string metaPath = Path.Combine(dir, Constants.CAMPAIGN_META_FILE_NAME);

                if (_FileIO.Exists(metaPath))
                {
                    campaigns.Add(_FileIO.LoadFromConfig<CampaignMeta>(metaPath));
                }
            }


            return campaigns;
        }
    }
}
