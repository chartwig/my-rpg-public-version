﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Zenject;
using DungeonLiege.Entities;

namespace DungeonLiege.Core
{
    public class CampaignManager : ICampaignManager
    {
        private IFileIO _FileIO { get; set; }
        private IDirectoryIO _DirectoryIO { get; set; }
        private IBoardManager _BoardManager { get; set; }

        [Inject]
        public void Init(IFileIO fileIO, IDirectoryIO directoryIO, IBoardManager boardManager)
        {
            _FileIO = fileIO;
            _DirectoryIO = directoryIO;
            _BoardManager = boardManager;
        }

        public void Save(string campaignName, string boardName, Dictionary<System.Guid, GameObject> sceneEntities, CampaignData campaignData)
        {
            string path = _FileIO.GetModulePath();

            if (!_DirectoryIO.Exists(path))
            {
                _DirectoryIO.Create(path);
            }

            string modulePath = Path.Combine(path, campaignName);
            string metaPath;

            if (!_DirectoryIO.Exists(modulePath))
            {
                _DirectoryIO.Create(modulePath);
            }

            metaPath = Path.Combine(modulePath, Constants.CAMPAIGN_FILE_NAME);

            if (campaignData.Boards.ContainsKey(boardName))
            {
                campaignData.Boards[boardName] = _BoardManager.Create(boardName, sceneEntities);
            }
            else
            {
                campaignData.Boards.Add(boardName, _BoardManager.Create(boardName, sceneEntities));
            }

            _FileIO.SaveToConfig<CampaignData>(metaPath, campaignData);
        }

        public CampaignData Load(string campaignToLoad)
        {
            string path = _FileIO.GetModulePath();

            if (!_DirectoryIO.Exists(path))
            {
                _DirectoryIO.Create(path);
            }

            string modulePath = Path.Combine(path, campaignToLoad);
            string dataPath;

            if (!_DirectoryIO.Exists(modulePath))
            {
                _DirectoryIO.Create(modulePath);
            }

            dataPath = Path.Combine(modulePath, Constants.CAMPAIGN_FILE_NAME);

            return _FileIO.LoadFromConfig<CampaignData>(dataPath);
        }

    }
}
