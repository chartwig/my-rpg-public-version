﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonLiege.Core
{
    public interface IFileIO
    {
        bool Exists(string filePath);
        T LoadFromConfig<T>(string configPath);
        void SaveToConfig<T>(string configPath, T source);
        string GetModulePath();
        string ReadAllText(string path);
        void WriteAllText(string path, string text);
    }
}
