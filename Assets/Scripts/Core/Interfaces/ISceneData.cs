﻿namespace DungeonLiege.Core
{
    public interface ISceneData
    {
        string CampaignToLoad { get; set; }
    }
}
