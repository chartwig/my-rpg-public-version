﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonLiege.Core{
    public interface IDirectoryIO
    {
        bool Exists(string directoryPath);
        int FileCount(string directoryPath);
        IEnumerable<string> EnumerateFiles(string directoryPath);
        IEnumerable<string> GetDirectories(string path);
        void Create(string directoryPath);
    }
}
