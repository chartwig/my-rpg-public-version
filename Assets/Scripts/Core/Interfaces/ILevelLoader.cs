﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonLiege.Core
{
    public interface ILevelLoader
    {
        void LoadLevel(int level);
    }
}
