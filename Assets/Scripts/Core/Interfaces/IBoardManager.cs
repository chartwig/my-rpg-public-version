﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DungeonLiege.Entities;

namespace DungeonLiege.Core
{
    public interface IBoardManager
    {
        Board Create(string boardName, Dictionary<System.Guid, GameObject> sceneEntities);
    }
}
