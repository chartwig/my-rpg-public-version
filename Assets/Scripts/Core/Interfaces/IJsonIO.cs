﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonLiege.Core {
    public interface IJsonIO
    {
        T FromJson<T>(string fileText);
        string ToJson<T>(T source);
    }
}
