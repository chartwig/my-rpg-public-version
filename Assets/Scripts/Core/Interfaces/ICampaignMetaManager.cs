﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DungeonLiege.Entities;

namespace DungeonLiege.Core {
    public interface ICampaignMetaManager
    {
        void Save(CampaignMeta metaData);
        CampaignMeta Load(string campaignMetaName);
        List<CampaignMeta> LoadAll();
    }
}
