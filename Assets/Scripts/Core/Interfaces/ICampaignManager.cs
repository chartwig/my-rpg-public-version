﻿using System.Collections.Generic;
using UnityEngine;
using DungeonLiege.Entities;

namespace DungeonLiege.Core
{
    public interface ICampaignManager
    {
        void Save(string campaignName, string boardName, Dictionary<System.Guid, GameObject> sceneEntities, CampaignData campaignData);
        CampaignData Load(string campaignToLoad);
    }
}
