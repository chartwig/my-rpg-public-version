﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DungeonLiege.Entities;
using DungeonLiege.UI;

namespace DungeonLiege.Core
{
    public class BoardManager : IBoardManager
    {
        public Board Create(string boardName, Dictionary<System.Guid, GameObject> sceneEntities)
        {
            Board newBoard = new Board();

            newBoard.Name = boardName;
            newBoard.Entities = new Dictionary<System.Guid, Entity>();

            foreach(KeyValuePair<System.Guid, GameObject> entity in sceneEntities){
                EntityProperties props = entity.Value.GetComponent<EntityProperties>();

                newBoard.Entities.Add(entity.Key, new Entity() {
                    AssetName = props.AssetName,
                    Position = new Position(entity.Value.transform.position),
                    Rotation = new Rotation(entity.Value.transform.rotation)
                });
            }

            return newBoard;
        }

    }
}
