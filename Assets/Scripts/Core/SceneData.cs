﻿using System.Collections;
using System.Collections.Generic;

namespace DungeonLiege.Core
{
    public class SceneData : ISceneData
    {
        public string CampaignToLoad { get; set; }
    }
}
