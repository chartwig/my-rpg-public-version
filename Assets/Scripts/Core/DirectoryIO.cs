﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace DungeonLiege.Core
{
    public class DirectoryIO : IDirectoryIO
    {
        /// <summary>
        /// Checks whether or not a directory exists
        /// </summary>
        /// <param name="directoryPath">The path to the directory</param>
        /// <returns><c>true</c>, if the file exists; otherwise, <c>false</c>.</returns>
        public bool Exists(string directoryPath)
        {
            return Directory.Exists(directoryPath);
        }
        /// <summary>
        /// Returns the count of the files within a directory
        /// </summary>
        /// <param name="directoryPath">The path to the directory</param>
        /// <returns>The count of the files within a directory</returns>
        public int FileCount(string directoryPath)
        {
            return Directory.GetFiles(directoryPath).Length;
        }
        /// <summary>
        /// Returns an IEnumerable collection of file names within a directory path
        /// </summary>
        /// <param name="directoryPath">The path to the directory</param>
        /// <returns>The IEnumerable collection of file names</returns>
        public IEnumerable<string> EnumerateFiles(string directoryPath)
        {
            return Directory.EnumerateFiles(directoryPath);
        }
        public IEnumerable<string> GetDirectories(string path){
            return Directory.GetDirectories(path);
        }
        public void Create(string directoryPath)
        {
            Directory.CreateDirectory(directoryPath);
        }
    }
}
