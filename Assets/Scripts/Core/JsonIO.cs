﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;

namespace DungeonLiege.Core{
    public class JsonIO : IJsonIO
    {
        public T FromJson<T>(string fileText){
            return JsonConvert.DeserializeObject<T>(fileText);
        }

        public string ToJson<T>(T source)
        {
            return JsonConvert.SerializeObject(source);
        }
    }
}
