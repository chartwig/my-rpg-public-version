﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Zenject;

namespace DungeonLiege.Core
{
    public class FileIO : IFileIO
    {
        public IJsonIO _JsonIO;

        [Inject]
        public FileIO(IJsonIO jsonIO)
        {
            _JsonIO = jsonIO;
        }
        /// <summary>
        /// Checks whether or not a file exists
        /// </summary>
        /// <param name="filePath">The path to the file</param>
        /// <returns><c>true</c>, if the file exists; otherwise, <c>false</c>.</returns>
        public bool Exists(string filePath)
        {
            return File.Exists(filePath);
        }
        public string ReadAllText(string path)
        {
            return File.ReadAllText(path);
        }
        public void WriteAllText(string path, string text)
        {
            File.WriteAllText(path, text);
        }
        /// <summary>
        /// Loads data from a JSON config file
        /// </summary>
        /// <typeparam name="T">The type of the data to load</typeparam>
        /// <param name="configPath">The path to the JSON config file</param>
        /// <returns>The data in its native type, or <c>default(T)</c>.</returns>
        public T LoadFromConfig<T>(string configPath)
        {
            if (this.Exists(configPath))
            {
                string tempConfig = this.ReadAllText(configPath);

                return _JsonIO.FromJson<T>(tempConfig);
            }

            return default;
        }
        public void SaveToConfig<T>(string configPath, T source)
        {
            string tempJson = _JsonIO.ToJson<T>(source);

            this.WriteAllText(configPath, tempJson);
        }
        /// <summary>
        /// Returnes the module path
        /// </summary>
        public string GetModulePath()
        {
            string path = Path.Combine(Application.dataPath, "Campaigns");

            // Better than putting test modules into the assets
#if UNITY_EDITOR
            path = "C:\\Campaigns";
#endif

            return path;
        }
    }
}
