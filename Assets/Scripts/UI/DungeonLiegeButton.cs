﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace DungeonLiege.UI
{
    public class DungeonLiegeButton : MonoBehaviour
    {
        public float left = 1f;
        public float right = 1f;
        public float top = 50f;
        public float height = 50f;

        // Use this for initialization
        void Start()
        {
        }

        public void SetButtonText(string text)
        {
            Text tempText;
            tempText = gameObject.GetComponentInChildren<Text>();
            tempText.text = text;
        }

        public void SetButtonClick(UnityAction action)
        {
            Button tempButton = gameObject.GetComponentInChildren<Button>();
            tempButton.onClick.AddListener(action);
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void SetAnchorPoints(float offsetMultiplier)
        {
            RectTransform rect = gameObject.GetComponent<RectTransform>();

            if (rect)
            {
                rect.anchorMin = new Vector2(0, 1);
                rect.anchorMax = new Vector2(1, 1);

                rect.offsetMin = new Vector2(left, -top*offsetMultiplier);
                rect.offsetMax = new Vector3(-right, rect.offsetMin.y+height);
            }
        }
    }
}
