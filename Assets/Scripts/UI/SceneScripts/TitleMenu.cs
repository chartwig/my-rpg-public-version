﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DungeonLiege.Core;
using Zenject;

namespace DungeonLiege.UI
{
    public class TitleMenu : MonoBehaviour
    {
        ILevelLoader _Loader { get; set; }

        private void Start()
        {
        }

        [Inject]
        public void Init(ILevelLoader loader){
            _Loader = loader;
        }

        public void LoadCampaignMenu()
        {
            _Loader.LoadLevel((int)Constants.Levels.CampaignMenu);
        }
        public void LoadEditorDebugMenu()
        {
            _Loader.LoadLevel((int)Constants.Levels.Editor);
        }
        public void LoadGilesDebugMenu()
        {
            _Loader.LoadLevel((int)Constants.Levels.Game);
        }
    }
}
