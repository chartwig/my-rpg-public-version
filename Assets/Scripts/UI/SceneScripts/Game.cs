﻿using DungeonLiege.Core;
using DungeonLiege.Entities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace DungeonLiege.UI
{
    public class Game : MonoBehaviour
    {
        ILevelLoader _Loader { get; set; }
        IJsonIO _JsonIO { get; set; }

        public GameObject PauseMenu;
        public GameObject LoadingScreen;
        public GameObject GameScreen;
        public CampaignDataManager CampaignDataManager;

        [Inject]
        public void Init(ILevelLoader loader, IJsonIO jsonIO){
            _Loader = loader;
            _JsonIO = jsonIO;
        }

        public void Start()
        {
            PauseMenu.SetActive(false);
        }

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                PauseMenu.SetActive(!PauseMenu.activeSelf);
            }

            if (CampaignDataManager.IsLoaded)
            {
                // Need nested if instead of an && because we should not load the campaign
                // if the campaign is already loaded. With an &&, if the campaign is loaded
                // but the editor screen is not active, it would reload the campaign.
                if (!GameScreen.activeSelf)
                {
                    ShowEditorScreen();
                }
            }
            else
            {
                LoadCampaign();
            }
        }

        public void LoadTitleMenu()
        {
            Debug.Log("Exiting...");
            _Loader.LoadLevel((int)Constants.Levels.TitleMenu);
        }

        public void LoadCampaign()
        {
            Debug.Log("Loading Campaign...");

            CampaignDataManager.LoadCampaign();
        }

        public void SaveCampaign()
        {
            Debug.Log("Saving Campaign...");

            CampaignDataManager.SaveCampaign();
        }

        public void Back()
        {
            Debug.Log("Back");
            PauseMenu.SetActive(false);
        }

        public void SaveAndExit()
        {
            SaveCampaign();
            LoadTitleMenu();
        }
        
        public void ShowLoadingScreen()
        {
            LoadingScreen.SetActive(true);
            GameScreen.SetActive(false);
        }

        public void ShowEditorScreen()
        {
            LoadingScreen.SetActive(false);
            GameScreen.SetActive(true);
        }

    }
}
