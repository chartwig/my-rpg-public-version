﻿using UnityEngine;
using DungeonLiege.Entities;

namespace DungeonLiege.UI {
    public class Editor : MonoBehaviour {
        public GameObject PauseMenu;
        public GameObject LoadingScreen;
        public GameObject EditorScreen;
        public CampaignDataManager CampaignDataManager;

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                PauseMenu.SetActive(!PauseMenu.activeSelf);
            }

            if (CampaignDataManager.IsLoaded)
            {
                // Need nested if instead of an && because we should not load the campaign
                // if the campaign is already loaded. With an &&, if the campaign is loaded
                // but the editor screen is not active, it would reload the campaign.
                if (!EditorScreen.activeSelf)
                {
                    ShowEditorScreen();
                }
            }
            else
            {
                CampaignDataManager.LoadCampaign();
            }
        }

        public void ShowLoadingScreen()
        {
            LoadingScreen.SetActive(true);
            EditorScreen.SetActive(false);
        }

        public void ShowEditorScreen()
        {
            LoadingScreen.SetActive(false);
            EditorScreen.SetActive(true);
        }
    }
}
