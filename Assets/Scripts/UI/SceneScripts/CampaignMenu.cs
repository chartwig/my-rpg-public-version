﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using DungeonLiege.UI;
using DungeonLiege.Entities;
using DungeonLiege.Core;

namespace DungeonLiege.UI
{
    public class CampaignMenu : MonoBehaviour
    {
        public List<CampaignMeta> CampaignMetas;
        public DungeonLiegeButton PrefabSelectButton;
        public DungeonLiegeButtonScrollViewContent ScrollViewContent;
        public CampaignMetaDisplayPanel displayPanel;

        private ILevelLoader _Loader { get; set; }
        private ICampaignMetaManager _CampaignMetaManager { get; set; }
        private ISceneData _SceneData { get; set; }

        // Use this for initialization
        void Start()
        {
            CampaignMetas = GetCampaigns();

            UpdateMetaList();
        }

        [Inject]
        public void Init(ILevelLoader loader, ICampaignMetaManager metaManager, ISceneData sceneData)
        {
            _Loader = loader;
            _CampaignMetaManager = metaManager;
            _SceneData = sceneData;
        }

        void SelectCampaign(CampaignMeta meta)
        {
            displayPanel.SetText(meta.Name, meta.Description);

            _SceneData.CampaignToLoad = meta.Name;
        }

        public void LoadTitleMenu()
        {
            _Loader.LoadLevel((int)Constants.Levels.TitleMenu);
        }

        public void LoadEditor()
        {
            _Loader.LoadLevel((int)Constants.Levels.Editor);
        }

        public void LoadGame()
        {
            _Loader.LoadLevel((int)Constants.Levels.Game);
        }

        public List<CampaignMeta> GetCampaigns()
        {
            return _CampaignMetaManager.LoadAll();
        }

        private void Update()
        {
        }

        public void AddCampaignMeta(CampaignMeta meta)
        {
            CampaignMetas.Add(meta);

            UpdateMetaList();
        }

        private void UpdateMetaList()
        {
            DungeonLiegeButton tempButton;
            float offsetCount = 0.0f;

            ScrollViewContent.ClearButtons();

            foreach (CampaignMeta meta in CampaignMetas)
            {
                if (!ScrollViewContent.Buttons.ContainsKey(meta.Name))
                {
                    offsetCount += 1.0f;
                    tempButton = Instantiate<DungeonLiegeButton>(PrefabSelectButton);

                    tempButton.SetButtonText(meta.Name);
                    tempButton.SetButtonClick(() => SelectCampaign(meta));

                    ScrollViewContent.AddButton(meta.Name, tempButton, offsetCount);

                }
            }
        }
    }
}
