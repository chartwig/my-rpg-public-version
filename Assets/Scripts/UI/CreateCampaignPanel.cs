﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DungeonLiege.Entities;
using UnityEngine.UI;
using DungeonLiege.Core;
using Zenject;

namespace DungeonLiege.UI
{
    public class CreateCampaignPanel : MonoBehaviour
    {
        public CampaignMenu CampaignMenu;
        public InputField CampaignNameField;
        public InputField CampaignDescriptionField;
        public InputField CampaignAuthorField;

        private ICampaignMetaManager _CampaignMetaManager { get; set; }
        private ICampaignManager _CampaignManager { get; set; }

        [Inject]
        public void Init(ICampaignMetaManager metaManager, ICampaignManager campaignManager)
        {
            _CampaignMetaManager = metaManager;
            _CampaignManager = campaignManager;
        }

        public void ClosePanel()
        {
            this.gameObject.SetActive(false);
        }

        public void CreateCampaign()
        {
            CampaignData campaignData = new CampaignData();
            CampaignMeta metaData = new CampaignMeta();

            // SEt this to values on the form
            metaData.Name = CampaignNameField.text;
            metaData.Description = CampaignDescriptionField.text;
            metaData.AuthorName = CampaignAuthorField.text;

            campaignData.Boards = new Dictionary<string, Board>();

            _CampaignMetaManager.Save(metaData);

            _CampaignManager.Save(metaData.Name, $"{metaData.Name} Board 1", new Dictionary<System.Guid, GameObject>(), campaignData);

            CampaignMenu.AddCampaignMeta(metaData);

            ClosePanel();
        }
    }
}
