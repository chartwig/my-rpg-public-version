﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonLiege.UI
{
    public class CreateCampaignPanelManager : MonoBehaviour
    {
        public CreateCampaignPanel CreateCampaignPanel;

        // Start is called before the first frame update
        void Start()
        {
            CreateCampaignPanel.gameObject.SetActive(false);
        }

        public void ShowCreateCampaignPanel()
        {
            CreateCampaignPanel.gameObject.SetActive(true);
        }
    }
}
