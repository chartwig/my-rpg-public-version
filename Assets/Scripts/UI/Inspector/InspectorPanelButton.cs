﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonLiege.UI
{
    public class InspectorPanelButton : MonoBehaviour
    {
        public GameObject InspectorPanel;

        public void ToggleInspectorPanel()
        {
            InspectorPanel.SetActive(!InspectorPanel.activeSelf);
        }
    }
}

