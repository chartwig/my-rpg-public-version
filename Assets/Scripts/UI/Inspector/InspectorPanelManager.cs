﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace DungeonLiege.UI
{
    public class InspectorPanelManager : MonoBehaviour
    {
        public EntitySelectionManager SelectionManager;
        public InspectorPanel InspectorPanel;
        // Start is called before the first frame update
        void Start()
        {
            InspectorPanel.gameObject.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {
            if (SelectionManager.SelectedGameObjects.Count > 0)
            {
                InspectorPanel.gameObject.SetActive(true);

                UpdateSnapAmount();

                GameObject firstSelection = SelectionManager.SelectedGameObjects.First();

                if (firstSelection != null)
                {
                    InspectorPanel.SelectedPosition = firstSelection.transform.position;
                    InspectorPanel.SelectedRotation = firstSelection.transform.rotation;
                }
            } else
            {
                InspectorPanel.gameObject.SetActive(false);
            }
        }

        public void UpdateSnapAmount()
        {
            // The slider allows 1/8th increments
            SelectionManager.SnapAmount = InspectorPanel.SnapAmountSlider.value / 8;
            SelectionManager.RotationSnapAmount = 90 * (InspectorPanel.RotationSnapAmountSlider.value / 8);
        }

    }
}
