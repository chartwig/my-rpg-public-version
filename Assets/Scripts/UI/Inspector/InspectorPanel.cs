﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DungeonLiege.UI
{
    public class InspectorPanel : MonoBehaviour
    {
        public Vector3 SelectedPosition;
        public Quaternion SelectedRotation;
        public Text PositionText;
        public Text RotationText;
        public Text SnapAmountText;
        public Slider SnapAmountSlider;
        public Text RotationSnapAmountText;
        public Slider RotationSnapAmountSlider;

        // Update is called once per frame
        void Update()
        {
            PositionText.text = $"POSITION: x: {SelectedPosition.x}, y: {SelectedPosition.y}, z: {SelectedPosition.z}";
            RotationText.text = $"ROTATION: x: {SelectedRotation.x}, y: {SelectedRotation.y}, z: {SelectedRotation.z}, w: {SelectedRotation.w}";

            SnapAmountText.text = $"Snap Amount: {SnapAmountSlider.value / 8}";
            RotationSnapAmountText.text = $"Rotation Snap Amount: {90 * (RotationSnapAmountSlider.value / 8)}";
        }

    }
}

