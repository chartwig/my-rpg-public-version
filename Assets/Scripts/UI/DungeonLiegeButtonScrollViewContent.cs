﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonLiege.UI
{
    public class DungeonLiegeButtonScrollViewContent : MonoBehaviour
    {
        public Dictionary<string, DungeonLiegeButton> Buttons;

        private void Start()
        {
            Buttons = new Dictionary<string, DungeonLiegeButton>();
        }

        public void ClearButtons()
        {
            if (Buttons.Count > 0)
            {
                foreach (DungeonLiegeButton button in Buttons.Values)
                {
                    GameObject.Destroy(button.gameObject);
                }
            }

            Buttons = new Dictionary<string, DungeonLiegeButton>();
        }

        public void AddButton(string name, DungeonLiegeButton button, float offsetMultiplier)
        {
            button.transform.parent = gameObject.transform;
            button.SetAnchorPoints(offsetMultiplier);
            Buttons.Add(name, button);
        }
    }
}
