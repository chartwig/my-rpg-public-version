﻿using DungeonLiege.Core;
using DungeonLiege.Entities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace DungeonLiege.UI
{
    public class PauseMenuPanel : MonoBehaviour
    {
        ILevelLoader _Loader { get; set; }
        public CampaignDataManager CampaignDataManager;

        [Inject]
        public void Init(ILevelLoader loader){
            _Loader = loader;
        }

        public void Start()
        {
            gameObject.SetActive(false);
        }

        public void LoadTitleMenu()
        {
            _Loader.LoadLevel((int)Constants.Levels.TitleMenu);
        }

        public void LoadCampaign()
        {
            CampaignDataManager.LoadCampaign();
        }

        public void SaveCampaign()
        {
            CampaignDataManager.SaveCampaign();
        }

        public void Back()
        {
            gameObject.SetActive(false);
        }

        public void SaveAndExit()
        {
            SaveCampaign();
            LoadTitleMenu();
        }

    }
}
