﻿using DungeonLiege.Entities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DungeonLiege.UI
{
    public class CampaignMetaDisplayPanel : MonoBehaviour
    {
        public CampaignMeta currentMeta;
        public Text metaTitleText;
        public Text metaDescText;

        public void SetText(string title, string description)
        {
            metaTitleText.text = title;
            metaDescText.text = description;
        }
    }
}
