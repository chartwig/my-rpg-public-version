﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DungeonLiege.Entities;

namespace DungeonLiege.UI
{
    public class EntityRowPanel : MonoBehaviour
    {
        public List<EntityItem> EntitiesForRow;
        public EntityItemButton Button;
        public List<EntityItemButton> Buttons;
        public int ButtonWidth = 80;
        public CampaignDataManager CampaignDataManager;

        public float left = 1f;
        public float right = 1f;
        public float top = 115f;
        public float height = 115f;

        // Start is called before the first frame update
        void Start()
        {
            Buttons = new List<EntityItemButton>();
            // Create a button for each ID that when clicked will put the scenery item in the center of the screen at some normal height?
            for(int i = 0; i < EntitiesForRow.Count; i++){
                EntityItem current = EntitiesForRow[i];

                EntityItemButton currentButton = CreateNewButton(current);

                AddButton(currentButton, (float)i);
            }
        }

        private EntityItemButton CreateNewButton(EntityItem entity)
        {
            
            EntityItemButton newButton = GameObject.Instantiate<EntityItemButton>(Button);

            newButton.Entity = entity;
            newButton.CampaignDataManager = CampaignDataManager;

            return newButton;
        }

        public void SetAnchorPoints(float offsetMultiplier)
        {
            RectTransform rect = gameObject.GetComponent<RectTransform>();

            if (rect)
            {
                rect.anchorMin = new Vector2(0, 1);
                rect.anchorMax = new Vector2(1, 1);

                rect.offsetMin = new Vector2(left, -top*offsetMultiplier);
                rect.offsetMax = new Vector3(-right, rect.offsetMin.y+height);
            }
        }

        public void AddButton(EntityItemButton button, float offsetMultiplier)
        {
            button.transform.SetParent(gameObject.transform, false);
            button.SetAnchorPoints(offsetMultiplier);
            Buttons.Add(button);
        }
    }
}
