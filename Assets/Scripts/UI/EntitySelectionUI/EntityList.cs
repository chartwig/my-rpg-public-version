﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DungeonLiege.Core;

namespace DungeonLiege.UI
{
    public class EntityList : MonoBehaviour
    {
        public Dictionary<string, EntityItem> Entities = new Dictionary<string, EntityItem>()
        {
            {
                "Door 2",
                new EntityItem() {
                    AssetPath = "PolygonDungeon/Prefabs/Environments/Walls/SM_Env_Door_02",
                    Name = "Door 2",
                    Category = Constants.ENTITY_CATEGORY_WALLS,
                    IconPath = "EntityIcons/SM_Env_Door_2"
                }
            },
            {
                "Round Door Frame",
                new EntityItem() {
                    AssetPath = "PolygonDungeon/Prefabs/Environments/Walls/SM_Env_Door_Frame_Round_01",
                    Name = "Round Door Frame",
                    Category = Constants.ENTITY_CATEGORY_WALLS,
                    IconPath = "EntityIcons/No_Icon"
                }
            },
            {
                "Large Tile Floor",
                new EntityItem() {
                    AssetPath = "PolygonDungeon/Prefabs/Environments/Floors/SM_Env_Tiles_03",
                    Name = "Large Tile Floor",
                    Category = Constants.ENTITY_CATEGORY_FLOORS,
                    IconPath = "EntityIcons/No_Icon"
                }
            },
            {
                "Brick Dungeon Wall",
                new EntityItem() {
                    AssetPath = "PolygonDungeon/Prefabs/Environments/Walls/SM_Env_Wall_01",
                    Name = "Brick Dungeon Wall",
                    Category = Constants.ENTITY_CATEGORY_WALLS,
                    IconPath = "EntityIcons/No_Icon"
                }
            },
            {
                "Stone Vessel",
                new EntityItem() {
                    AssetPath = "PolygonDungeon/Prefabs/Environments/Misc/SM_Env_Stone_Vessel_01",
                    Name = "Stone Vessel",
                    Category = Constants.ENTITY_CATEGORY_PROPS,
                    IconPath = "EntityIcons/No_Icon"
                }
            }
        };
    }
}
