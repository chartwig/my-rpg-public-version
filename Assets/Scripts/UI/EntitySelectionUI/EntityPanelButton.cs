﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonLiege.UI
{
    public class EntityPanelButton : MonoBehaviour
    {
        public GameObject entityPanel;

        public void ToggleSceneryPanel()
        {
            entityPanel.SetActive(!entityPanel.activeSelf);
        }
    }
}

