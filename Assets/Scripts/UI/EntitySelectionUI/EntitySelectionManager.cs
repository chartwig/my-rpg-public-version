﻿using DungeonLiege.Entities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonLiege.UI
{
    public class EntitySelectionManager : MonoBehaviour
    {
        public List<GameObject> SelectedGameObjects;
        public float SnapAmount;
        public float RotationSnapAmount;
        public CampaignDataManager BoardManager;

        // Update is called once per frame
        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    GameObject selected = hit.transform.gameObject;

                    if (selected != null && !SelectedGameObjects.Contains(selected))
                    {
                        Renderer selectedRenderer = selected.GetComponent<Renderer>();

                        if (selectedRenderer)
                        {
                            selectedRenderer.material.shader = Shader.Find("Shader Graphs/Highlighted");
                        }

                        SelectedGameObjects.Add(selected);
                    }
                }
            }
            else if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.D))
            {
                if (SelectedGameObjects.Count > 0)
                {
                    MoveSelected();
                }
            } else if (Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.E))
            {
                if (SelectedGameObjects.Count > 0)
                {
                    RotateSelected();
                }
            }
        }
        public void Deselect()
        {
            SelectedGameObjects.ForEach((GameObject current) =>
            {
                Renderer selectedRenderer = current.GetComponent<Renderer>();

                if (selectedRenderer)
                {
                    selectedRenderer.material.shader = Shader.Find("Universal Render Pipeline/Lit");
                }
            });

            SelectedGameObjects.Clear();
        }
        public void DeleteSelected()
        {
            SelectedGameObjects.ForEach((GameObject current) =>
            {
                EntityProperties properties = current.GetComponent<EntityProperties>();

                if (BoardManager.SceneEntities.ContainsKey(properties.EntityId))
                {
                    BoardManager.SceneEntities.Remove(properties.EntityId);
                }

                GameObject.Destroy(current);
            });

            SelectedGameObjects.Clear();
        }
        private void MoveSelected()
        {

            float zAxisSpeed = GetSpeedForward() + GetSpeedBackward();
            float yAxisSpeed = GetSpeedLeftward() + GetSpeedRightward();
            float xAxisSpeed = GetSpeedUpward() + GetSpeedDownward();

            SelectedGameObjects.ForEach((GameObject current) =>
            {
                current.transform.Translate(xAxisSpeed, yAxisSpeed, zAxisSpeed);
            });

        }
        private void RotateSelected()
        {
            float xRotationSpeed = (GetZRotationSpeedForward() + GetZRotationSpeedBackward());
            float yRotationSpeed = (GetYRotationSpeedForward() + GetYRotationSpeedBackward());
            float zRotationSpeed = (GetXRotationSpeedForward() + GetXRotationSpeedBackward());

            SelectedGameObjects.ForEach((GameObject current) => {
                current.transform.Rotate(xRotationSpeed, yRotationSpeed, zRotationSpeed);
            });
        }
        private float GetSpeedUpward()
        {
            return Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.W) ? -SnapAmount : 0;
        }
        private float GetSpeedDownward()
        {
            return Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.S) ? SnapAmount : 0;
        }
        private float GetSpeedRightward()
        {
            return Input.GetKeyDown(KeyCode.D) ? SnapAmount : 0;
        }
        private float GetSpeedLeftward()
        {
            return Input.GetKeyDown(KeyCode.A) ? -SnapAmount : 0;
        }
        private float GetSpeedForward()
        {
            return !Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.W) ? SnapAmount : 0;
        }
        private float GetSpeedBackward()
        {
            return !Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.S) ? -SnapAmount : 0;
        }

        private float GetXRotationSpeedForward()
        {
            return Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.E) ? RotationSnapAmount : 0;
        }
        private float GetXRotationSpeedBackward()
        {
            return Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.Q) ? -RotationSnapAmount : 0;
        }

        private float GetZRotationSpeedForward()
        {
            return !Input.GetKey(KeyCode.LeftControl) && !Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.E) ? -RotationSnapAmount : 0;
        }
        private float GetZRotationSpeedBackward()
        {
            return !Input.GetKey(KeyCode.LeftControl) && !Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.Q) ? RotationSnapAmount : 0;
        }

        private float GetYRotationSpeedForward()
        {
            return !Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.E) ? -RotationSnapAmount : 0;
        }
        private float GetYRotationSpeedBackward()
        {
            return !Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.Q) ? RotationSnapAmount : 0;
        }
    }
}

