﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonLiege.UI
{
    public struct EntityItem
    {
        // Display name
        public string Name; 
        // For filtering? Maybe this needs to be elsewhere?
        public string Category; // Need like a const of standard names, that way I can easily get names using autocomplete, but modders can add new categories.
        public string IconPath;
        public string AssetPath;
    }
}
