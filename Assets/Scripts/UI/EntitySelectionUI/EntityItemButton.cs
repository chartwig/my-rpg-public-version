﻿using UnityEngine;
using UnityEngine.UI;
using DungeonLiege.Entities;
using System.Collections.Generic;

namespace DungeonLiege.UI
{
    public class EntityItemButton : MonoBehaviour
    {
        public EntityItem Entity;
        public GameObject Image;
        public CampaignDataManager CampaignDataManager;

        public float left = 80f;
        public float right = 1f;
        public float top = 1f;
        public float width = 80f;

        private void Start()
        {
            Image image = Image.GetComponent<Image>();

            image.sprite = Resources.Load<Sprite>(Entity.IconPath);

            Debug.Log($"Button created for {Entity.Name} from the {Entity.Category} category.");
            Debug.Log($"Image loaded from {Entity.IconPath} for {Entity.Name}");
        }

        public void SelectEntityButton()
        {
            // 15, 0, 15 is the center of the iso screen
            Position position = new Position(new Vector3(15, 0, 15));
            Rotation rotation = new Rotation(Quaternion.identity);

            Debug.Log($"Button pressed for {Entity.Name}");

            KeyValuePair<System.Guid, Entity> newEntity = new KeyValuePair<System.Guid, Entity>(System.Guid.NewGuid(), new Entity()
            {
                AssetName = Entity.Name,
                Position = position,
                Rotation = rotation
            });

            // Handles loading into the scene
            CampaignDataManager.LoadEntity(newEntity);
        }

        public void SetAnchorPoints(float offsetMultiplier)
        {
            RectTransform rect = gameObject.GetComponent<RectTransform>();

            if (rect)
            {
                rect.anchorMin = new Vector2(0, 0);
                rect.anchorMax = new Vector2(0, 1);

                rect.offsetMin = new Vector2(left*offsetMultiplier, top);
                rect.offsetMax = new Vector2(rect.offsetMin.x+width, top);
            }
        }
    }
}
