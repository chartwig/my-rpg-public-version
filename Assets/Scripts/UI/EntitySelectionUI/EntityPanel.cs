﻿using DungeonLiege.Entities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace DungeonLiege.UI
{
    public class EntityPanel : MonoBehaviour
    {
        public short ButtonsPerRow = 10;
        public EntityRowPanel EntityRowPanel;
        public EntityList EntityList;
        public EntityPanelScrollViewContent ScrollViewContent;
        public CampaignDataManager CampaignDataManager;
        // Start is called before the first frame update
        void Start()
        {
            List<EntityItem> currentEntities = new List<EntityItem>();
            EntityRowPanel newPanel;
            int rowOffsetValue = 1;

            for(int i = 0; i < EntityList.Entities.Count; i += ButtonsPerRow)
            {
                // This will get the range of IDs from the list, from the current row count start to one minus 
                currentEntities = EntityList.Entities.Values.ToList<EntityItem>().GetRange(i, Mathf.Min(ButtonsPerRow, EntityList.Entities.Count - i));

                newPanel = CreateNewPanel(currentEntities);
                newPanel.CampaignDataManager = CampaignDataManager;

                ScrollViewContent.AddPanel(newPanel, (float) rowOffsetValue);

                rowOffsetValue += 1;
            }
        }

        EntityRowPanel CreateNewPanel(List<EntityItem> entities)
        {
            EntityRowPanel newPanel = GameObject.Instantiate<EntityRowPanel>(EntityRowPanel);

            newPanel.EntitiesForRow = entities;

            return newPanel;
        }

        //List<string> GetCurrentIDs(int startIndex)
        //{
        //    List<string> newIDs = new List<string>();

        //    for(int i = startIndex; i < startIndex + ButtonsPerRow; i++)
        //    {
        //        if(i > EntityList.Entities.Count)
        //        {
        //            break;
        //        }

        //        newIDs.Add(EntityList.Entities[i]);
        //    }

        //    return newIDs;
        //}
    }
}
