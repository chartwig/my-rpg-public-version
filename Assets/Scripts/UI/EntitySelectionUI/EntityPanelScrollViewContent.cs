﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonLiege.UI
{
    public class EntityPanelScrollViewContent : MonoBehaviour
    {
        public List<EntityRowPanel> Panels;

        private void Start()
        {
            Panels = new List<EntityRowPanel>();
        }

        public void AddPanel(EntityRowPanel panel, float offsetMultiplier)
        {
            panel.transform.SetParent(gameObject.transform, false);
            panel.SetAnchorPoints(offsetMultiplier);
            Panels.Add(panel);
        }
    }
}
