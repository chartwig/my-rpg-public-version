﻿using DungeonLiege.Core;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Zenject;
using System.Linq;
using DungeonLiege.UI;

namespace DungeonLiege.Entities
{
    public class CampaignDataManager : MonoBehaviour
    {
        public CampaignInformation Campaign;
        public string BoardName;
        public EntityList EntityList;
        public Dictionary<System.Guid, GameObject> SceneEntities = new Dictionary<System.Guid, GameObject>();
        public bool IsLoaded;

        private IFileIO _FileIO { get; set; }
        private IDirectoryIO _DirectoryIO { get; set; }
        private ICampaignMetaManager _CampaignMetaManager { get; set; }
        private ICampaignManager _CampaignManager { get; set; }
        private IBoardManager _BoardManager { get; set; }
        private ISceneData _SceneData { get; set; }

        [Inject]
        public void Init(IFileIO fileIO, IDirectoryIO directoryIO, ICampaignMetaManager campaignMetaManager, ICampaignManager campaignManager, IBoardManager boardManager, ISceneData sceneData)
        {
            _FileIO = fileIO;
            _DirectoryIO = directoryIO;
            _CampaignMetaManager = campaignMetaManager;
            _CampaignManager = campaignManager;
            _SceneData = sceneData;

            IsLoaded = false;
        }

        public void SaveCampaign()
        {
            SaveCampaign(Campaign.Meta, Campaign.Data);
        }

        public void SaveCampaign(CampaignMeta campaignMeta, CampaignData campaignData)
        {
            SaveCampaignMeta(campaignMeta);
            SaveCampaignData(campaignMeta.Name, campaignData);
        }


        public void SaveCampaignData(string campaignName, CampaignData campaignData)
        {
            _CampaignManager.Save(campaignName, BoardName, SceneEntities, campaignData);
        }

        public Board CreateBoard()
        {
            return _BoardManager.Create(BoardName, SceneEntities);
        }

        public void LoadCampaign()
        {
            IsLoaded = false;

            Board currentBoard;
            LoadCampaignMeta();

            LoadCampaignData();

            if (!Campaign.Data.Equals(default(CampaignData)) && Campaign.Data.Boards.Count > 0)
            {
                currentBoard = Campaign.Data.Boards.Values.First<Board>();

                BoardName = currentBoard.Name;

                // Load the board into the scene
                foreach (KeyValuePair<System.Guid, Entity> entity in currentBoard.Entities)
                {
                    LoadEntity(entity);
                }
            } else
            {
                BoardName = "New Board";

                Campaign.Data.Boards = new Dictionary<string, Board>();
            }

            Debug.Log($"Campaign '{_SceneData.CampaignToLoad}' Loaded!");

            IsLoaded = true;
        }

        public void LoadCampaignData()
        {
            Campaign.Data = _CampaignManager.Load(_SceneData.CampaignToLoad);
        }

        public void LoadEntity(KeyValuePair<System.Guid, Entity> boardEntity)
        {
            if (EntityList.Entities.ContainsKey(boardEntity.Value.AssetName)){
                string entityPath = EntityList.Entities[boardEntity.Value.AssetName].AssetPath;
                GameObject entity = Resources.Load<GameObject>(entityPath);

                if (entity)
                {
                    Debug.Log($"Asset loaded from {entityPath}");

                    GameObject editorObject = GameObject.Instantiate(entity, boardEntity.Value.Position.GetByVector3(), boardEntity.Value.Rotation.GetByQuaternion());

                    MeshCollider objCollider = editorObject.AddComponent<MeshCollider>();

                    EntityProperties newEntityProperties = editorObject.AddComponent<EntityProperties>();

                    newEntityProperties.AssetName = boardEntity.Value.AssetName;

                    newEntityProperties.EntityId = boardEntity.Key;

                    SceneEntities.Add(boardEntity.Key, editorObject);
                } else
                {
                    Debug.Log("Entity not loaded");
                }
            }
        }

        public void SaveCampaignMeta(CampaignMeta metaData)
        {
            _CampaignMetaManager.Save(metaData);
        }

        public void LoadCampaignMeta()
        {
            _CampaignMetaManager.Load(_SceneData.CampaignToLoad);
        }

        public void CreateBoard(string name)
        {
            Board board = new Board();
            board.Entities = new Dictionary<System.Guid, Entity>();

            Campaign.Data.Boards.Add(name, board);
        }
    }
}
