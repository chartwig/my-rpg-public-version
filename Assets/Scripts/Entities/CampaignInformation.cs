﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonLiege.Entities
{
    public struct CampaignInformation {
        public CampaignData Data;
        public CampaignMeta Meta;
    }
}
