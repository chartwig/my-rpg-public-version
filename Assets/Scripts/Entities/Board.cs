﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonLiege.Entities
{
    public struct Board
    {
        public string Name { get; set; }
        public Dictionary<System.Guid, Entity> Entities { get; set; }
    }
}
