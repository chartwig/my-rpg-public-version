﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonLiege.Entities
{
    public struct Entity
    {
        public Position Position;
        public Rotation Rotation;
        public string AssetName;
    }
}
