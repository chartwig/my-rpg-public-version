﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonLiege.Entities
{
    public struct CampaignMeta
    {
        public string Name;
        public string Description;
        public string AuthorName;
    }
}
