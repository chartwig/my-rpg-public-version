﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonLiege.Entities
{
    public struct Rotation
    {
        public float x;
        public float y;
        public float z;
        public float w;

        public Rotation(Quaternion newRotation)
        {
            this.x = newRotation.x;
            this.y = newRotation.y;
            this.z = newRotation.z;
            this.w = newRotation.w;
        }

        public Quaternion GetByQuaternion()
        {
            return new Quaternion(this.x, this.y, this.z, this.w);
        }
    }
}
