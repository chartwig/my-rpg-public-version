﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace DungeonLiege.Entities
{
    public struct CampaignData
    {
        public Dictionary<string, Board> Boards { get; set; }
    }
}
