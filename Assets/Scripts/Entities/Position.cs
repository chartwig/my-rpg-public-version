﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonLiege.Entities
{
    public struct Position
    {
        public float x;
        public float y;
        public float z;

        public Position(Vector3 newPosition)
        {
            this.x = newPosition.x;
            this.y = newPosition.y;
            this.z = newPosition.z;
        }

        public Vector3 GetByVector3()
        {
            return new Vector3(this.x, this.y, this.z);
        }
    }
}
