﻿using LiteNetLib.Utils;

namespace MultiplayerARPG
{
    public struct GameMessage : INetSerializable
    {
        public enum Type : byte
        {
            None,
            InvalidItemData,
            NotFoundCharacter,
            NotAbleToLoot,
            NotEnoughGold,
            NotEnoughItems,
            CannotCarryAnymore,
            // Equip
            CannotEquip,
            LevelOrAttributeNotEnough,
            InvalidEquipPositionRightHand,
            InvalidEquipPositionLeftHand,
            InvalidEquipPositionRightHandOrLeftHand,
            InvalidEquipPositionArmor,
            // Refine
            CannotRefine,
            RefineItemReachedMaxLevel,
            RefineSuccess,
            RefineFail,
            // Enhance
            CannotEnhanceSocket,
            NotEnoughSocketEnchaner,
            // Repair
            CannotRepair,
            RepairSuccess,
            // Dealing
            CharacterIsInAnotherDeal,
            CharacterIsTooFar,
            CannotAcceptDealingRequest,
            DealingRequestDeclined,
            InvalidDealingState,
            DealingCanceled,
            // Party
            PartyInvitationDeclined,
            CannotSendPartyInvitation,
            CannotKickPartyMember,
            CannotKickYourSelfFromParty,
            CannotKickPartyLeader,
            JoinedAnotherParty,
            NotJoinedParty,
            NotPartyLeader,
            CharacterJoinedAnotherParty,
            CharacterNotJoinedParty,
            PartyMemberReachedLimit,
            // Guild
            GuildInvitationDeclined,
            CannotSendGuildInvitation,
            CannotKickGuildMember,
            CannotKickYourSelfFromGuild,
            CannotKickGuildLeader,
            CannotKickHigherGuildMember,
            JoinedAnotherGuild,
            NotJoinedGuild,
            NotGuildLeader,
            CharacterJoinedAnotherGuild,
            CharacterNotJoinedGuild,
            GuildMemberReachedLimit,
            GuildRoleNotAvailable,
            GuildSkillReachedMaxLevel,
            NoGuildSkillPoint,
            // Game Data
            UnknowGameDataTitle,
            UnknowGameDataDescription,
            // Bank
            NotEnoughGoldToDeposit,
            NotEnoughGoldToWithdraw,
            CannotAccessStorage,
            // Combatant
            NoAmmo,
        }
        public Type type;

        public void Deserialize(NetDataReader reader)
        {
            type = (Type)reader.GetByte();
        }

        public void Serialize(NetDataWriter writer)
        {
            writer.Put((byte)type);
        }
    }
}
