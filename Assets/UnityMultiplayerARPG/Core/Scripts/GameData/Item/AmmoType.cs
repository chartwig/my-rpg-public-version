﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MultiplayerARPG
{
    [CreateAssetMenu(fileName = "Ammo Type", menuName = "Create GameData/Ammo Type", order = -4894)]
    public partial class AmmoType : BaseGameData
    {
    }
}
