﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MultiplayerARPG
{
    public enum MonsterCharacteristic
    {
        Normal,
        Aggressive,
        Assist,
    }

    [System.Serializable]
    public struct MonsterCharacterAmount
    {
        public MonsterCharacter monster;
        public short amount;
    }

    [CreateAssetMenu(fileName = "Monster Character", menuName = "Create GameData/Monster Character", order = -4998)]
    public sealed partial class MonsterCharacter : BaseCharacter
    {
        [Header("Monster Data")]
        public short defaultLevel = 1;
        [Tooltip("`Normal` will attack when being attacked, `Aggressive` will attack when enemy nearby, `Assist` will attack when other with same `Ally Id` being attacked.")]
        public MonsterCharacteristic characteristic;
        [Tooltip("This will work with assist characteristic only, to detect ally")]
        public ushort allyId;
        [Tooltip("This move speed will be applies when it's wandering. if it's going to chase enemy, stats'moveSpeed will be applies")]
        public float wanderMoveSpeed;
        [Tooltip("Range to see an enemy")]
        public float visualRange = 5f;
        [SerializeField]
        private MonsterSkill[] monsterSkills;
        [HideInInspector]
        public float deadHideDelay = 2f;
        [HideInInspector]
        public float deadRespawnDelay = 5f;

        [Header("Weapon/Attack Abilities")]
        public DamageInfo damageInfo;
        public DamageIncremental damageAmount;

        [Header("Killing Rewards")]
        public int randomExpMin;
        public int randomExpMax;
        public int randomGoldMin;
        public int randomGoldMax;
        public byte maxDropItems = 5;
        public ItemDrop[] randomItems;
        public ItemDropTable itemDropTable;

        private CharacterStatsIncremental? adjustStats;
        private AttributeIncremental[] adjustAttributes;
        private ResistanceIncremental[] adjustResistances;

        public override sealed CharacterStatsIncremental Stats
        {
            get
            {
                // Adjust base stats by default level
                if (defaultLevel <= 1)
                {
                    return base.Stats;
                }
                else
                {
                    if (adjustStats.HasValue)
                    {
                        adjustStats = new CharacterStatsIncremental()
                        {
                            baseStats = base.Stats.baseStats + (base.Stats.statsIncreaseEachLevel * -(defaultLevel - 1)),
                            statsIncreaseEachLevel = base.Stats.statsIncreaseEachLevel,
                        };
                    }
                    return adjustStats.Value;
                }
            }
        }

        public override sealed AttributeIncremental[] Attributes
        {
            get
            {
                // Adjust base attributes by default level
                if (defaultLevel <= 1)
                {
                    return base.Attributes;
                }
                else
                {
                    if (adjustAttributes == null)
                    {
                        adjustAttributes = new AttributeIncremental[base.Attributes.Length];
                        AttributeIncremental tempValue;
                        for (int i = 0; i < base.Attributes.Length; ++i)
                        {
                            tempValue = base.Attributes[i];
                            adjustAttributes[i] = new AttributeIncremental()
                            {
                                attribute = tempValue.attribute,
                                amount = new IncrementalShort()
                                {
                                    baseAmount = (short)(tempValue.amount.baseAmount + (tempValue.amount.amountIncreaseEachLevel * -(defaultLevel - 1))),
                                    amountIncreaseEachLevel = tempValue.amount.amountIncreaseEachLevel,
                                }
                            };
                        }
                    }
                    return adjustAttributes;
                }
            }
        }

        public override sealed ResistanceIncremental[] Resistances
        {
            get
            {
                // Adjust base resistances by default level
                if (defaultLevel <= 1)
                {
                    return base.Resistances;
                }
                else
                {
                    if (adjustResistances == null)
                    {
                        adjustResistances = new ResistanceIncremental[base.Resistances.Length];
                        ResistanceIncremental tempValue;
                        for (int i = 0; i < base.Resistances.Length; ++i)
                        {
                            tempValue = base.Resistances[i];
                            adjustResistances[i] = new ResistanceIncremental()
                            {
                                damageElement = tempValue.damageElement,
                                amount = new IncrementalFloat()
                                {
                                    baseAmount = (short)(tempValue.amount.baseAmount + (tempValue.amount.amountIncreaseEachLevel * -(defaultLevel - 1))),
                                    amountIncreaseEachLevel = tempValue.amount.amountIncreaseEachLevel,
                                }
                            };
                        }
                    }
                    return adjustResistances;
                }
            }
        }

        private Dictionary<Skill, short> cacheSkillLevels;
        public override Dictionary<Skill, short> CacheSkillLevels
        {
            get
            {
                if (cacheSkillLevels == null)
                    cacheSkillLevels = GameDataHelpers.CombineSkills(monsterSkills, new Dictionary<Skill, short>());
                return cacheSkillLevels;
            }
        }

        public int RandomExp()
        {
            int min = randomExpMin;
            int max = randomExpMax;
            if (min > max)
                min = max;
            return Random.Range(min, max);
        }

        public int RandomGold()
        {
            int min = randomGoldMin;
            int max = randomGoldMax;
            if (min > max)
                min = max;
            return Random.Range(min, max);
        }

        public void RandomItems(System.Action<Item, short> onRandomItem)
        {
            int countDrops = 0;
            ItemDrop randomItem;
            int loopCounter;

            for (loopCounter = 0; loopCounter < randomItems.Length && countDrops < maxDropItems; ++loopCounter)
            {
                ++countDrops;
                randomItem = randomItems[loopCounter];
                if (randomItem.item == null ||
                    randomItem.amount == 0 ||
                    !GameInstance.Items.ContainsKey(randomItem.item.DataId) ||
                    Random.value > randomItem.dropRate)
                    continue;

                onRandomItem.Invoke(randomItem.item, randomItem.amount);
            }

            // Random drop item from table
            if (itemDropTable != null && countDrops < maxDropItems)
            {
                for (loopCounter = 0; loopCounter < itemDropTable.randomItems.Length && countDrops < maxDropItems; ++loopCounter)
                {
                    ++countDrops;
                    randomItem = itemDropTable.randomItems[loopCounter];
                    if (randomItem.item == null ||
                        randomItem.amount == 0 ||
                        !GameInstance.Items.ContainsKey(randomItem.item.DataId) ||
                        Random.value > randomItem.dropRate)
                        continue;

                    onRandomItem.Invoke(randomItem.item, randomItem.amount);
                }
            }
        }

        public bool RandomSkill(BaseMonsterCharacterEntity entity, out Skill skill, out short level)
        {
            skill = null;
            level = 1;
            float random = Random.value;
            foreach (MonsterSkill monsterSkill in monsterSkills)
            {
                if (monsterSkill.skill == null)
                    continue;

                if (random < monsterSkill.useRate && entity.HpRate < monsterSkill.useWhenHpRate)
                {
                    skill = monsterSkill.skill;
                    level = monsterSkill.level;
                    return true;
                }
            }
            return false;
        }
    }
}
