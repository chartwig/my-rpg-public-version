﻿namespace MultiplayerARPG
{
    public interface IJumppableModel
    {
        void PlayJumpAnimation();
    }
}
