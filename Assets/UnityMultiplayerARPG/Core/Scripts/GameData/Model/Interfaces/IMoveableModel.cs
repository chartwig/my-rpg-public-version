﻿namespace MultiplayerARPG
{
    public interface IMoveableModel
    {
        void SetMoveAnimationSpeedMultiplier(float moveAnimationSpeedMultiplier);
        void SetMovementState(MovementState movementState);
    }
}
