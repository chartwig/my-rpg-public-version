﻿namespace MultiplayerARPG
{
    public interface ICharacterModel2D
    {
        DirectionType2D CurrentDirectionType { get; set; }
    }
}
