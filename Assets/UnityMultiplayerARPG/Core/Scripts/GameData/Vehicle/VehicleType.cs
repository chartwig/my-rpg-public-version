﻿using UnityEngine;

namespace MultiplayerARPG
{
    [CreateAssetMenu(fileName = "Vehicle Type", menuName = "Create GameData/Vehicle Type", order = -4893)]
    public partial class VehicleType : BaseGameData
    {
    }
}
