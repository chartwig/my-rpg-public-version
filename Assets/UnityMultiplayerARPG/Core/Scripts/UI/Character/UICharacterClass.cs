﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MultiplayerARPG
{
    public class UICharacterClass : UISelectionEntry<BaseCharacter>
    {
        [Header("String Formats")]
        [Tooltip("Format => {0} = {Title}")]
        public UILocaleKeySetting formatKeyTitle = new UILocaleKeySetting(UILocaleKeys.UI_FORMAT_SIMPLE);
        [Tooltip("Format => {0} = {Description}")]
        public UILocaleKeySetting formatKeyDescription = new UILocaleKeySetting(UILocaleKeys.UI_FORMAT_SIMPLE);

        [Header("UI Elements")]
        public TextWrapper uiTextTitle;
        public TextWrapper uiTextDescription;
        public Image imageIcon;
        public UICharacterStats uiStats;
        public UIAttributeAmounts uiAttributes;
        public UIResistanceAmounts uiResistances;
        public UISkillLevels uiSkills;

        protected override void UpdateData()
        {
            if (uiTextTitle != null)
            {
                uiTextTitle.text = string.Format(
                    LanguageManager.GetText(formatKeyTitle),
                    Data == null ? LanguageManager.GetUnknowTitle() : Data.Title);
            }

            if (uiTextDescription != null)
            {
                uiTextDescription.text = string.Format(
                    LanguageManager.GetText(formatKeyDescription),
                    Data == null ? LanguageManager.GetUnknowDescription() : Data.Description);
            }

            if (imageIcon != null)
            {
                Sprite iconSprite = Data == null ? null : Data.icon;
                imageIcon.gameObject.SetActive(iconSprite != null);
                imageIcon.sprite = iconSprite;
            }

            if (uiStats != null)
                uiStats.Data = Data.GetCharacterStats(1);

            if (uiAttributes != null)
                uiAttributes.Data = Data.GetCharacterAttributes(1);

            if (uiResistances != null)
                uiResistances.Data = Data.GetCharacterResistances(1);

            if (uiSkills != null)
                uiSkills.Data = Data.CacheSkillLevels;
        }
    }
}
