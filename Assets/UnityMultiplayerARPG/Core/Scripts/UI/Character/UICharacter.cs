﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace MultiplayerARPG
{
    public partial class UICharacter : UISelectionEntry<ICharacterData>
    {
        [Header("String Formats")]
        [Tooltip("Format => {0} = {Character Name}")]
        public UILocaleKeySetting formatKeyName = new UILocaleKeySetting(UILocaleKeys.UI_FORMAT_SIMPLE);
        [Tooltip("Format => {0} = {Level}")]
        public UILocaleKeySetting formatKeyLevel = new UILocaleKeySetting(UILocaleKeys.UI_FORMAT_LEVEL);
        [Tooltip("Format => {0} = {Stat Points}")]
        public UILocaleKeySetting formatKeyStatPoint = new UILocaleKeySetting(UILocaleKeys.UI_FORMAT_STAT_POINTS);
        [Tooltip("Format => {0} = {Skill Points}")]
        public UILocaleKeySetting formatKeySkillPoint = new UILocaleKeySetting(UILocaleKeys.UI_FORMAT_SKILL_POINTS);
        [Tooltip("Format => {0} = {Gold Amount}")]
        public UILocaleKeySetting formatKeyGold = new UILocaleKeySetting(UILocaleKeys.UI_FORMAT_GOLD);
        [Tooltip("Format => {0} = {Current Total Weights}, {1} = {Weight Limit}")]
        public UILocaleKeySetting formatKeyWeightLimitStats = new UILocaleKeySetting(UILocaleKeys.UI_FORMAT_CURRENT_WEIGHT);
        [Tooltip("Format => {0} = {Min Damage}, {1} = {Max Damage}")]
        public UILocaleKeySetting formatKeyWeaponDamage = new UILocaleKeySetting(UILocaleKeys.UI_FORMAT_DAMAGE_AMOUNT);

        [Header("UI Elements")]
        public TextWrapper uiTextName;
        public TextWrapper uiTextLevel;
        // EXP
        [HideInInspector] // TODO: This is deprecated, it will be removed later
        public TextWrapper uiTextExp;
        [HideInInspector] // TODO: This is deprecated, it will be removed later
        public Image imageExpGage;
        public UIGageValue uiGageExp;
        // HP
        [HideInInspector] // TODO: This is deprecated, it will be removed later
        public TextWrapper uiTextHp;
        [HideInInspector] // TODO: This is deprecated, it will be removed later
        public Image imageHpGage;
        public UIGageValue uiGageHp;
        // MP
        [HideInInspector] // TODO: This is deprecated, it will be removed later
        public TextWrapper uiTextMp;
        [HideInInspector] // TODO: This is deprecated, it will be removed later
        public Image imageMpGage;
        public UIGageValue uiGageMp;
        // Stamina
        [HideInInspector] // TODO: This is deprecated, it will be removed later
        public TextWrapper uiTextStamina;
        [HideInInspector] // TODO: This is deprecated, it will be removed later
        public Image imageStaminaGage;
        public UIGageValue uiGageStamina;
        // Food
        [HideInInspector] // TODO: This is deprecated, it will be removed later
        public TextWrapper uiTextFood;
        [HideInInspector] // TODO: This is deprecated, it will be removed later
        public Image imageFoodGage;
        public UIGageValue uiGageFood;
        // Water
        [HideInInspector] // TODO: This is deprecated, it will be removed later
        public TextWrapper uiTextWater;
        [HideInInspector] // TODO: This is deprecated, it will be removed later
        public Image imageWaterGage;
        public UIGageValue uiGageWater;

        public TextWrapper uiTextStatPoint;
        public TextWrapper uiTextSkillPoint;
        public TextWrapper uiTextGold;
        public TextWrapper uiTextWeightLimit;
        public TextWrapper uiTextWeaponDamages;
        public UIDamageElementAmounts uiRightHandDamages;
        public UIDamageElementAmounts uiLeftHandDamages;
        public UICharacterStats uiCharacterStats;
        public UICharacterBuffs uiCharacterBuffs;
        public UIResistanceAmounts uiCharacterResistances;
        public UICharacterAttributePair[] uiCharacterAttributes;
        public UICharacterClass uiCharacterClass;

        [Header("Options")]
        public bool showStatsWithBuffs;
        public bool showResistanceWithBuffs;
        public bool showAttributeWithBuffs;
        public bool showDamageWithBuffs;

        // Improve garbage collector
        private float cacheWeightLimit;
        private CharacterStats cacheStats;
        private Dictionary<Attribute, short> cacheAttributes;
        private Dictionary<DamageElement, float> cacheResistances;
        private Dictionary<DamageElement, MinMaxFloat> cacheIncreaseDamages;
        private Dictionary<EquipmentSet, int> cacheEquipmentSets;
        // Cache bonus data
        private CharacterStats bonusStats;
        private Dictionary<Attribute, short> bonusAttributes;
        private Dictionary<DamageElement, float> bonusResistances;
        private Dictionary<DamageElement, MinMaxFloat> bonusIncreaseDamages;
        private Dictionary<Skill, short> bonusSkills;

        private Dictionary<Attribute, UICharacterAttribute> cacheUICharacterAttributes;
        public Dictionary<Attribute, UICharacterAttribute> CacheUICharacterAttributes
        {
            get
            {
                if (cacheUICharacterAttributes == null)
                {
                    cacheUICharacterAttributes = new Dictionary<Attribute, UICharacterAttribute>();
                    foreach (UICharacterAttributePair uiCharacterAttribute in uiCharacterAttributes)
                    {
                        if (uiCharacterAttribute.attribute != null &&
                            uiCharacterAttribute.ui != null &&
                            !cacheUICharacterAttributes.ContainsKey(uiCharacterAttribute.attribute))
                            cacheUICharacterAttributes.Add(uiCharacterAttribute.attribute, uiCharacterAttribute.ui);
                    }
                }
                return cacheUICharacterAttributes;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            MigrateUIGageValue();
        }

        protected void OnValidate()
        {
#if UNITY_EDITOR
            if (MigrateUIGageValue())
                EditorUtility.SetDirty(this);
#endif
        }

        private bool MigrateUIGageValue()
        {
            return UIGageValue.Migrate(ref uiGageExp, ref uiTextExp, ref imageExpGage) ||
                UIGageValue.Migrate(ref uiGageHp, ref uiTextHp, ref imageHpGage) ||
                UIGageValue.Migrate(ref uiGageMp, ref uiTextMp, ref imageMpGage) ||
                UIGageValue.Migrate(ref uiGageStamina, ref uiTextStamina, ref imageStaminaGage) ||
                UIGageValue.Migrate(ref uiGageFood, ref uiTextFood, ref imageFoodGage) ||
                UIGageValue.Migrate(ref uiGageWater, ref uiTextWater, ref imageWaterGage);
        }

        protected override void Update()
        {
            base.Update();

            Profiler.BeginSample("UICharacter - Update UI (Immediately)");
            // Hp
            int currentHp = 0;
            int maxHp = 0;
            if (Data != null)
            {
                currentHp = Data.CurrentHp;
                maxHp = Data.CacheMaxHp;
            }
            if (uiGageHp != null)
                uiGageHp.Update(currentHp, maxHp);

            // Mp
            int currentMp = 0;
            int maxMp = 0;
            if (Data != null)
            {
                currentMp = Data.CurrentMp;
                maxMp = Data.CacheMaxMp;
            }
            if (uiGageMp != null)
                uiGageMp.Update(currentMp, maxMp);

            // Stamina
            int currentStamina = 0;
            int maxStamina = 0;
            if (Data != null)
            {
                currentStamina = Data.CurrentStamina;
                maxStamina = Data.CacheMaxStamina;
            }
            if (uiGageStamina != null)
                uiGageStamina.Update(currentStamina, maxStamina);

            // Food
            int currentFood = 0;
            int maxFood = 0;
            if (Data != null)
            {
                currentFood = Data.CurrentFood;
                maxFood = Data.CacheMaxFood;
            }
            if (uiGageFood != null)
                uiGageFood.Update(currentFood, maxFood);

            // Water
            int currentWater = 0;
            int maxWater = 0;
            if (Data != null)
            {
                currentWater = Data.CurrentWater;
                maxWater = Data.CacheMaxWater;
            }
            if (uiGageWater != null)
                uiGageWater.Update(currentWater, maxWater);

            Profiler.EndSample();
        }

        protected override void UpdateUI()
        {
            Profiler.BeginSample("UICharacter - Update UI");

            if (uiTextName != null)
            {
                uiTextName.text = string.Format(
                    LanguageManager.GetText(formatKeyName),
                    Data == null ? LanguageManager.GetUnknowTitle() : Data.DisplayCharacterName);
            }

            if (uiTextLevel != null)
            {
                uiTextLevel.text = string.Format(
                    LanguageManager.GetText(formatKeyLevel),
                    Data == null ? "1" : Data.DisplayLevel.ToString("N0"));
            }

            int[] expTree = GameInstance.Singleton.ExpTree;
            int currentExp = 0;
            int nextLevelExp = 0;
            if (Data != null && Data.GetNextLevelExp() > 0)
            {
                currentExp = Data.Exp;
                nextLevelExp = Data.GetNextLevelExp();
            }
            else if (Data != null && Data.Level - 2 > 0 && Data.Level - 2 < expTree.Length)
            {
                int maxExp = expTree[Data.Level - 2];
                currentExp = maxExp;
                nextLevelExp = maxExp;
            }
            if (uiGageExp != null)
                uiGageExp.Update(currentExp, nextLevelExp);

            // Player character data
            IPlayerCharacterData playerCharacter = Data as IPlayerCharacterData;
            if (uiTextStatPoint != null)
            {
                uiTextStatPoint.text = string.Format(
                    LanguageManager.GetText(formatKeyStatPoint),
                    playerCharacter == null ? "0" : playerCharacter.StatPoint.ToString("N0"));
            }

            if (uiTextSkillPoint != null)
            {
                uiTextSkillPoint.text = string.Format(
                    LanguageManager.GetText(formatKeySkillPoint),
                    playerCharacter == null ? "0" : playerCharacter.SkillPoint.ToString("N0"));
            }

            if (uiTextGold != null)
            {
                uiTextGold.text = string.Format(
                    LanguageManager.GetText(formatKeyGold),
                    playerCharacter == null ? "0" : playerCharacter.Gold.ToString("N0"));
            }

            BaseCharacter character = Data == null ? null : Data.GetDatabase();
            if (uiCharacterClass != null)
                uiCharacterClass.Data = character;

            Profiler.EndSample();
        }

        protected override void UpdateData()
        {
            cacheStats = showStatsWithBuffs ? Data.GetStats() : Data.GetStats(true, false);
            cacheAttributes = showAttributeWithBuffs ? Data.GetAttributes() : Data.GetAttributes(true, false);
            cacheResistances = showResistanceWithBuffs ? Data.GetResistances() : Data.GetResistances(true, false);
            cacheIncreaseDamages = showDamageWithBuffs ? Data.GetIncreaseDamages() : Data.GetIncreaseDamages(true, false);
            cacheWeightLimit = cacheStats.weightLimit;
            if (!showStatsWithBuffs)
            {
                // Always increase weight limit by buff bonus because it should always show real weight limit in UIs
                cacheWeightLimit += Data.GetBuffStats().weightLimit;
            }

            if (bonusAttributes == null)
                bonusAttributes = new Dictionary<Attribute, short>();
            if (bonusResistances == null)
                bonusResistances = new Dictionary<DamageElement, float>();
            if (bonusIncreaseDamages == null)
                bonusIncreaseDamages = new Dictionary<DamageElement, MinMaxFloat>();
            if (bonusSkills == null)
                bonusSkills = new Dictionary<Skill, short>();
            if (cacheEquipmentSets == null)
                cacheEquipmentSets = new Dictionary<EquipmentSet, int>();

            Data.GetEquipmentSetBonus(out bonusStats, bonusAttributes, bonusResistances, bonusIncreaseDamages, bonusSkills, cacheEquipmentSets);
            // Increase stats by equipment set bonus
            cacheStats += bonusStats;
            cacheAttributes = GameDataHelpers.CombineAttributes(cacheAttributes, bonusAttributes);
            cacheResistances = GameDataHelpers.CombineResistances(cacheResistances, bonusResistances);
            cacheIncreaseDamages = GameDataHelpers.CombineDamages(cacheIncreaseDamages, bonusIncreaseDamages);
            cacheWeightLimit += bonusStats.weightLimit;

            if (uiTextWeightLimit != null)
            {
                uiTextWeightLimit.text = string.Format(
                    LanguageManager.GetText(formatKeyWeightLimitStats),
                    GameInstance.Singleton.GameplayRule.GetTotalWeight(Data).ToString("N2"),
                    cacheWeightLimit.ToString("N2"));
            }

            CharacterItem rightHandItem = Data.EquipWeapons.rightHand;
            CharacterItem leftHandItem = Data.EquipWeapons.leftHand;
            Item rightHandWeapon = rightHandItem.GetWeaponItem();
            Item leftHandWeapon = leftHandItem.GetWeaponItem();
            Dictionary<DamageElement, MinMaxFloat> rightHandDamages = rightHandWeapon != null ? GameDataHelpers.CombineDamages(cacheIncreaseDamages, rightHandWeapon.GetDamageAmount(rightHandItem.level, rightHandItem.GetEquipmentBonusRate(), Data)) : null;
            Dictionary<DamageElement, MinMaxFloat> leftHandDamages = leftHandWeapon != null ? GameDataHelpers.CombineDamages(cacheIncreaseDamages, leftHandWeapon.GetDamageAmount(leftHandItem.level, leftHandItem.GetEquipmentBonusRate(), Data)) : null;

            if (uiTextWeaponDamages != null)
            {
                string textDamages = "";
                if (rightHandWeapon != null)
                {
                    MinMaxFloat sumDamages = GameDataHelpers.GetSumDamages(rightHandDamages);
                    if (!string.IsNullOrEmpty(textDamages))
                        textDamages += "\n";
                    textDamages += string.Format(
                        LanguageManager.GetText(formatKeyWeaponDamage),
                        sumDamages.min.ToString("N0"),
                        sumDamages.max.ToString("N0"));
                }
                if (leftHandWeapon != null)
                {
                    MinMaxFloat sumDamages = GameDataHelpers.GetSumDamages(leftHandDamages);
                    if (!string.IsNullOrEmpty(textDamages))
                        textDamages += "\n";
                    textDamages += string.Format(
                        LanguageManager.GetText(formatKeyWeaponDamage),
                        sumDamages.min.ToString("N0"),
                        sumDamages.max.ToString("N0"));
                }
                if (rightHandWeapon == null && leftHandWeapon == null)
                {
                    Item defaultWeaponItem = GameInstance.Singleton.DefaultWeaponItem;
                    WeaponItemEquipType defaultWeaponItemType = defaultWeaponItem.EquipType;
                    KeyValuePair<DamageElement, MinMaxFloat> damageAmount = defaultWeaponItem.GetDamageAmount(1, 1f, Data);
                    textDamages = string.Format(
                        LanguageManager.GetText(formatKeyWeaponDamage),
                        damageAmount.Value.min.ToString("N0"),
                        damageAmount.Value.max.ToString("N0"));
                }
                uiTextWeaponDamages.text = textDamages;
            }

            if (uiRightHandDamages != null)
            {
                if (rightHandWeapon == null)
                    uiRightHandDamages.Hide();
                else
                {
                    uiRightHandDamages.Show();
                    uiRightHandDamages.Data = rightHandDamages;
                }
            }

            if (uiLeftHandDamages != null)
            {
                if (leftHandWeapon == null)
                    uiLeftHandDamages.Hide();
                else
                {
                    uiLeftHandDamages.Show();
                    uiLeftHandDamages.Data = leftHandDamages;
                }
            }

            if (uiCharacterStats != null)
                uiCharacterStats.Data = cacheStats;

            if (uiCharacterResistances != null)
                uiCharacterResistances.Data = cacheResistances;

            if (CacheUICharacterAttributes.Count > 0 && Data != null)
            {
                CharacterAttribute tempCharacterAttribute;
                Attribute tempAttribute;
                short tempAmount;
                IList<CharacterAttribute> characterAttributes = Data.Attributes;
                for (int indexOfData = 0; indexOfData < characterAttributes.Count; ++indexOfData)
                {
                    tempCharacterAttribute = characterAttributes[indexOfData];
                    tempAttribute = tempCharacterAttribute.GetAttribute();
                    UICharacterAttribute cacheUICharacterAttribute;
                    tempAmount = 0;
                    if (CacheUICharacterAttributes.TryGetValue(tempAttribute, out cacheUICharacterAttribute))
                    {
                        if (cacheAttributes.ContainsKey(tempAttribute))
                            tempAmount = cacheAttributes[tempAttribute];
                        cacheUICharacterAttribute.Setup(new CharacterAttributeTuple(tempCharacterAttribute, tempAmount), Data, indexOfData);
                        cacheUICharacterAttribute.Show();
                    }
                }
            }

            if (uiCharacterBuffs != null)
                uiCharacterBuffs.UpdateData(Data);
        }
    }
}
