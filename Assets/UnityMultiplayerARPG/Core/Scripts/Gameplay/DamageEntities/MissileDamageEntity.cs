﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using LiteNetLibManager;
using LiteNetLib;

namespace MultiplayerARPG
{
    public class MissileDamageEntity : BaseDamageEntity
    {
        public UnityEvent onExploded;
        public UnityEvent onDestroy;
        [Tooltip("If this value more than 0, when it hit anything or it is out of life, it will explode and apply damage to characters in this distance")]
        public float explodeDistance;

        protected float missileDistance;
        [SerializeField]
        protected SyncFieldFloat missileSpeed = new SyncFieldFloat();
        [SerializeField]
        protected SyncFieldBool isExploded = new SyncFieldBool();
        [SerializeField]
        protected SyncFieldPackedUInt lockingTargetId;

        protected IDamageableEntity lockingTarget;
        public IDamageableEntity LockingTarget
        {
            get
            {
                if (lockingTarget == null && lockingTargetId.Value > 0)
                    TryGetEntityByObjectId(lockingTargetId.Value, out lockingTarget);
                return lockingTarget;
            }
            set
            {
                if (!IsServer)
                    return;
                lockingTargetId.Value = value != null ? value.ObjectId : 0;
                lockingTarget = value;
            }
        }

        private Rigidbody cacheRigidbody;
        public Rigidbody CacheRigidbody
        {
            get
            {
                if (cacheRigidbody == null)
                    cacheRigidbody = GetComponent<Rigidbody>();
                return cacheRigidbody;
            }
        }

        private Rigidbody2D cacheRigidbody2D;
        public Rigidbody2D CacheRigidbody2D
        {
            get
            {
                if (cacheRigidbody2D == null)
                    cacheRigidbody2D = GetComponent<Rigidbody2D>();
                return cacheRigidbody2D;
            }
        }

        private float launchTime;
        private float missileDuration;
        private bool destroying;

        protected override void EntityAwake()
        {
            base.EntityAwake();
            gameObject.layer = 2;   // Ignore raycast
        }

        protected override void SetupNetElements()
        {
            base.SetupNetElements();
            missileSpeed.deliveryMethod = DeliveryMethod.ReliableOrdered;
            missileSpeed.syncMode = LiteNetLibSyncField.SyncMode.ServerToClients;
            isExploded.deliveryMethod = DeliveryMethod.ReliableOrdered;
            isExploded.syncMode = LiteNetLibSyncField.SyncMode.ServerToClients;
        }

        public override void OnSetup()
        {
            base.OnSetup();
            SetupNetElements();
        }

        private void OnIsExplodedChanged(bool isExploded)
        {
            if (isExploded)
            {
                if (onExploded != null)
                    onExploded.Invoke();
            }
        }

        public void SetupDamage(
            IAttackerEntity attacker,
            CharacterItem weapon,
            Dictionary<DamageElement, MinMaxFloat> allDamageAmounts,
            CharacterBuff debuff,
            uint hitEffectsId,
            float missileDistance,
            float missileSpeed,
            IDamageableEntity lockingTarget)
        {
            SetupDamage(attacker, weapon, allDamageAmounts, debuff, hitEffectsId);
            this.missileDistance = missileDistance;
            this.missileSpeed.Value = missileSpeed;

            if (missileDistance <= 0 && missileSpeed <= 0)
            {
                // Explode immediately when distance and speed is 0
                Explode();
                NetworkDestroy(destroyDelay);
                destroying = true;
                return;
            }

            LockingTarget = lockingTarget;
            launchTime = Time.unscaledTime;
            missileDuration = missileDistance / missileSpeed;
        }

        protected override void EntityUpdate()
        {
            if (destroying)
                return;

            base.EntityUpdate();
            if (Time.unscaledTime - launchTime > missileDuration)
            {
                Explode();
                NetworkDestroy(destroyDelay);
                destroying = true;
            }
        }

        protected override void EntityFixedUpdate()
        {
            base.EntityFixedUpdate();
            // Don't move if exploded
            if (isExploded.Value)
            {
                if (gameInstance.DimensionType == DimensionType.Dimension2D)
                {
                    if (CacheRigidbody2D != null)
                        CacheRigidbody2D.velocity = Vector2.zero;
                }
                else
                {
                    if (CacheRigidbody != null)
                        CacheRigidbody.velocity = Vector3.zero;
                }
                return;
            }

            // Turn to locking target position
            if (LockingTarget != null)
            {
                // Lookat target then do anything when it's in range
                Vector3 lookAtDirection = (LockingTarget.transform.position - CacheTransform.position).normalized;
                // slerp to the desired rotation over time
                if (lookAtDirection.magnitude > 0)
                {
                    Vector3 lookRotationEuler = Quaternion.LookRotation(lookAtDirection).eulerAngles;
                    lookRotationEuler.x = 0;
                    lookRotationEuler.z = 0;
                    CacheTransform.rotation = Quaternion.Euler(lookRotationEuler);
                }
            }

            if (gameInstance.DimensionType == DimensionType.Dimension2D)
            {
                if (CacheRigidbody2D != null)
                    CacheRigidbody2D.velocity = -CacheTransform.up * missileSpeed.Value;
            }
            else
            {
                if (CacheRigidbody != null)
                    CacheRigidbody.velocity = CacheTransform.forward * missileSpeed.Value;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            TriggerEnter(other.gameObject);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            TriggerEnter(other.gameObject);
        }

        private void TriggerEnter(GameObject other)
        {
            if (destroying)
                return;

            if (attacker == null || attacker.gameObject == other)
                return;

            IDamageableEntity target = null;
            if (FindTargetEntity(other, out target))
            {
                if (explodeDistance > 0f)
                {
                    // Explode immediately when hit something
                    Explode();
                }
                else
                {
                    // If this is not going to explode, just apply damage to target
                    ApplyDamageTo(target);
                }
                NetworkDestroy(destroyDelay);
                destroying = true;
                return;
            }

            // Don't hits: TransparentFX, IgnoreRaycast, character, item
            if (other.layer != 1 &&
                other.layer != 2 &&
                other.layer != gameInstance.characterLayer &&
                other.layer != gameInstance.itemDropLayer)
            {
                if (explodeDistance > 0f)
                {
                    // Explode immediately when hit something
                    Explode();
                }
                NetworkDestroy(destroyDelay);
                destroying = true;
                return;
            }
        }

        private bool FindTargetEntity(GameObject other, out IDamageableEntity target)
        {
            target = null;

            if (!IsServer)
                return false;

            if (attacker == null || attacker.gameObject == other)
                return false;

            target = other.GetComponent<IDamageableEntity>();

            if (target == null || target.IsDead() || !target.CanReceiveDamageFrom(attacker))
                return false;

            if (LockingTarget != null && LockingTarget != target)
                return false;

            return true;
        }

        private bool FindAndApplyDamage(GameObject other)
        {
            IDamageableEntity target;
            if (FindTargetEntity(other, out target))
            {
                ApplyDamageTo(target);
                return true;
            }
            return false;
        }

        private void Explode()
        {
            if (isExploded.Value || !IsServer)
                return;
            isExploded.Value = true;
            // Explode when distance > 0
            if (explodeDistance <= 0f)
                return;
            if (gameInstance.DimensionType == DimensionType.Dimension2D)
            {
                Collider2D[] colliders2D = Physics2D.OverlapCircleAll(CacheTransform.position, explodeDistance);
                foreach (Collider2D collider in colliders2D)
                {
                    FindAndApplyDamage(collider.gameObject);
                }
            }
            else
            {
                Collider[] colliders = Physics.OverlapSphere(CacheTransform.position, explodeDistance);
                foreach (Collider collider in colliders)
                {
                    FindAndApplyDamage(collider.gameObject);
                }
            }
        }

        public override void OnNetworkDestroy(byte reasons)
        {
            base.OnNetworkDestroy(reasons);
            if (reasons == LiteNetLibGameManager.DestroyObjectReasons.RequestedToDestroy)
            {
                if (onDestroy != null)
                    onDestroy.Invoke();
            }
        }
    }
}
