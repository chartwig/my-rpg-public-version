﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;
using LiteNetLibManager;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace MultiplayerARPG
{
    [RequireComponent(typeof(CharacterModelManager))]
    [RequireComponent(typeof(CharacterRecoveryComponent))]
    [RequireComponent(typeof(CharacterSkillAndBuffComponent))]
    public abstract partial class BaseCharacterEntity : DamageableEntity, ICharacterData, IAttackerEntity
    {
        public const float ACTION_COMMAND_DELAY = 0.2f;
        public const float RESPAWN_GROUNDED_CHECK_DURATION = 1f;
        public const int OVERLAP_COLLIDER_SIZE_FOR_ATTACK = 256;
        public const int OVERLAP_COLLIDER_SIZE_FOR_FIND = 32;

        protected struct SyncListRecachingState
        {
            public static readonly SyncListRecachingState Empty = new SyncListRecachingState();
            public bool isRecaching;
            public LiteNetLibSyncList.Operation operation;
            public int index;
        }
        
        [HideInInspector, System.NonSerialized]
        // This will be TRUE when this character enter to safe area
        public bool isInSafeArea;

        #region Serialize data
        [HideInInspector]
        // TODO: This will be made as private variable later
        public BaseCharacter database;
        [HideInInspector]
        // TODO: This will be removed later
        public Transform uiElementTransform;
        [HideInInspector]
        // TODO: This will be removed later
        public Transform miniMapElementContainer;

        [Header("Character Settings")]
        [Tooltip("When character attack with melee weapon, it will cast sphere from this transform to detect hit objects")]
        public Transform meleeDamageTransform;
        [Tooltip("When character attack with range weapon, it will spawn missile damage entity from this transform")]
        public Transform missileDamageTransform;
        [Tooltip("Character UI will instantiates to this transform")]
        public Transform characterUITransform;
        [Tooltip("Mini Map UI will instantiates to this transform")]
        public Transform miniMapUITransform;
        #endregion

#if UNITY_EDITOR
        [Header("Character Attack Debug")]
        public Vector3? debugDamagePosition;
        public Quaternion? debugDamageRotation;
        public Color debugFovColor = new Color(0, 1, 0, 0.04f);
#endif

        #region Protected data
        protected UICharacterEntity uiCharacterEntity;
        protected BaseGameEntity targetEntity;
        protected readonly Dictionary<string, int> equipItemIndexes = new Dictionary<string, int>();
        protected AnimActionType animActionType;
        public bool isAttackingOrUsingSkill { get; protected set; }
        public bool isCastingSkillCanBeInterrupted { get; protected set; }
        public bool isCastingSkillInterrupted { get; protected set; }
        public float castingSkillDuration { get; protected set; }
        public float castingSkillCountDown { get; protected set; }
        public float moveSpeedRateWhileAttackOrUseSkill { get; protected set; }
        public float respawnGroundedCheckCountDown { get; protected set; }
        #endregion

        #region Temp data
        protected Collider[] overlapColliders_ForAttackFunctions = new Collider[OVERLAP_COLLIDER_SIZE_FOR_ATTACK];
        protected Collider2D[] overlapColliders2D_ForAttackFunctions = new Collider2D[OVERLAP_COLLIDER_SIZE_FOR_ATTACK];
        protected Collider[] overlapColliders_ForFindFunctions = new Collider[OVERLAP_COLLIDER_SIZE_FOR_FIND];
        protected Collider2D[] overlapColliders2D_ForFindFunctions = new Collider2D[OVERLAP_COLLIDER_SIZE_FOR_FIND];
        protected GameObject tempGameObject;
        protected bool tempEnableMovement;
        #endregion

        public override sealed int MaxHp { get { return CacheMaxHp; } }
        public override sealed float MoveAnimationSpeedMultiplier { get { return gameplayRule.GetMoveSpeed(this) / CacheBaseMoveSpeed; } }
        public abstract int DataId { get; set; }
        public CharacterHitBox[] HitBoxes { get; protected set; }
        public bool HasAimPosition { get; protected set; }
        public Vector3 AimPosition { get; protected set; }

        private CharacterModelManager modelManager;
        public CharacterModelManager ModelManager
        {
            get
            {
                if (modelManager == null)
                    modelManager = GetComponent<CharacterModelManager>();
                if (modelManager == null)
                    modelManager = gameObject.AddComponent<CharacterModelManager>();
                return modelManager;
            }
        }

        public BaseCharacterModel CharacterModel
        {
            get { return ModelManager.ActiveModel; }
        }

        public Transform MeleeDamageTransform
        {
            get
            {
                if (meleeDamageTransform == null)
                    meleeDamageTransform = CacheTransform;
                return meleeDamageTransform;
            }
        }

        public Transform MissileDamageTransform
        {
            get
            {
                if (missileDamageTransform == null)
                    missileDamageTransform = MeleeDamageTransform;
                return missileDamageTransform;
            }
        }

        public Transform CharacterUITransform
        {
            get
            {
                if (characterUITransform == null)
                    characterUITransform = CacheTransform;
                return characterUITransform;
            }
        }

        public Transform MiniMapUITransform
        {
            get
            {
                if (miniMapUITransform == null)
                    miniMapUITransform = CacheTransform;
                return miniMapUITransform;
            }
        }

        protected override void EntityAwake()
        {
            base.EntityAwake();
            gameObject.layer = gameInstance.characterLayer;
            animActionType = AnimActionType.None;
            isRecaching = true;
            MigrateTransforms();
            HitBoxes = GetComponentsInChildren<CharacterHitBox>();
        }

        protected override void OnValidate()
        {
            base.OnValidate();
#if UNITY_EDITOR
            if (MigrateTransforms())
                EditorUtility.SetDirty(this);

            if (model != ModelManager.ActiveModel)
            {
                model = ModelManager.ActiveModel;
                EditorUtility.SetDirty(this);
            }
#endif
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            if (debugDamagePosition.HasValue && debugDamageRotation.HasValue)
            {
                float atkHalfFov = GetAttackFov(false) * 0.5f;
                float atkDist = GetAttackDistance(false);
                Handles.color = debugFovColor;
                Handles.DrawSolidArc(debugDamagePosition.Value, debugDamageRotation.Value * Vector3.up, debugDamageRotation.Value * Vector3.forward, -atkHalfFov, atkDist);
                Handles.DrawSolidArc(debugDamagePosition.Value, debugDamageRotation.Value * Vector3.up, debugDamageRotation.Value * Vector3.forward, atkHalfFov, atkDist);
                Handles.DrawSolidArc(debugDamagePosition.Value, debugDamageRotation.Value * Vector3.right, debugDamageRotation.Value * Vector3.forward, -atkHalfFov, atkDist);
                Handles.DrawSolidArc(debugDamagePosition.Value, debugDamageRotation.Value * Vector3.right, debugDamageRotation.Value * Vector3.forward, atkHalfFov, atkDist);

                Handles.color = new Color(1, 0, 0, debugFovColor.a);
                Handles.DrawSolidArc(debugDamagePosition.Value, debugDamageRotation.Value * Vector3.up, debugDamageRotation.Value * Vector3.forward, -atkHalfFov, 0);
                Handles.DrawSolidArc(debugDamagePosition.Value, debugDamageRotation.Value * Vector3.up, debugDamageRotation.Value * Vector3.forward, atkHalfFov, 0);
                Handles.DrawSolidArc(debugDamagePosition.Value, debugDamageRotation.Value * Vector3.right, debugDamageRotation.Value * Vector3.forward, -atkHalfFov, 0);
                Handles.DrawSolidArc(debugDamagePosition.Value, debugDamageRotation.Value * Vector3.right, debugDamageRotation.Value * Vector3.forward, atkHalfFov, 0);

                Handles.color = new Color(debugFovColor.r, debugFovColor.g, debugFovColor.b);
                Handles.DrawWireArc(debugDamagePosition.Value, debugDamageRotation.Value * Vector3.up, debugDamageRotation.Value * Vector3.forward, -atkHalfFov, atkDist);
                Handles.DrawWireArc(debugDamagePosition.Value, debugDamageRotation.Value * Vector3.up, debugDamageRotation.Value * Vector3.forward, atkHalfFov, atkDist);
                Handles.DrawWireArc(debugDamagePosition.Value, debugDamageRotation.Value * Vector3.up, debugDamageRotation.Value * Vector3.forward, -atkHalfFov, 0);
                Handles.DrawWireArc(debugDamagePosition.Value, debugDamageRotation.Value * Vector3.up, debugDamageRotation.Value * Vector3.forward, atkHalfFov, 0);

                Handles.DrawWireArc(debugDamagePosition.Value, debugDamageRotation.Value * Vector3.right, debugDamageRotation.Value * Vector3.forward, -atkHalfFov, 0);
                Handles.DrawWireArc(debugDamagePosition.Value, debugDamageRotation.Value * Vector3.right, debugDamageRotation.Value * Vector3.forward, atkHalfFov, 0);
                Handles.DrawWireArc(debugDamagePosition.Value, debugDamageRotation.Value * Vector3.right, debugDamageRotation.Value * Vector3.forward, -atkHalfFov, atkDist);
                Handles.DrawWireArc(debugDamagePosition.Value, debugDamageRotation.Value * Vector3.right, debugDamageRotation.Value * Vector3.forward, atkHalfFov, atkDist);
            }
        }
#endif

        private bool MigrateTransforms()
        {
            bool hasChanges = false;
            if (uiElementTransform != null)
            {
                characterUITransform = uiElementTransform;
                uiElementTransform = null;
                hasChanges = true;
            }
            if (miniMapElementContainer != null)
            {
                miniMapUITransform = miniMapElementContainer;
                miniMapElementContainer = null;
                hasChanges = true;
            }
            return hasChanges;
        }

        protected override void EntityUpdate()
        {
            Profiler.BeginSample("BaseCharacterEntity - Update");
            MakeCaches();

            tempEnableMovement = true;

            if (PassengingVehicleEntity != null)
                tempEnableMovement = false;

            if (respawnGroundedCheckCountDown <= 0)
            {
                if (gameInstance.DimensionType == DimensionType.Dimension3D &&
                    gameManager != null && CacheTransform.position.y <= gameManager.CurrentMapInfo.deadY)
                {
                    if (IsServer && !IsDead())
                    {
                        // Character will dead only when dimension type is 3D
                        CurrentHp = 0;
                        Killed(this);
                    }
                    // Disable movement when character dead
                    tempEnableMovement = false;
                }
            }
            else
            {
                respawnGroundedCheckCountDown -= Time.deltaTime;
            }

            // Clear data when character dead
            if (IsDead())
            {
                // Clear action states when character dead
                animActionType = AnimActionType.None;
                isAttackingOrUsingSkill = false;
                InterruptCastingSkill();
                ExitVehicle();
            }

            if (Movement != null && Movement.enabled != tempEnableMovement)
                Movement.enabled = tempEnableMovement;

            // Update character model handler based on passenging vehicle
            ModelManager.UpdatePassengingVehicle(PassengingVehicleType, PassengingVehicle.seatIndex);
            // Update current model
            model = ModelManager.ActiveModel;
            // Update is dead state
            CharacterModel.SetIsDead(IsDead());
            // Update move speed multiplier
            CharacterModel.SetMoveAnimationSpeedMultiplier(MoveAnimationSpeedMultiplier);
            // Update movement animation
            CharacterModel.SetMovementState(MovementState);
            // Update casting skill count down, will show gage at clients
            if (castingSkillCountDown > 0)
            {
                castingSkillCountDown -= Time.deltaTime;
                if (castingSkillCountDown < 0)
                    castingSkillCountDown = 0;
            }
            // Update direction type if character model is character model 2D
            if (CharacterModel is ICharacterModel2D)
            {
                // Set current direction to character model 2D
                (CharacterModel as ICharacterModel2D).CurrentDirectionType = CurrentDirectionType;
            }
            Profiler.EndSample();
        }

        #region Relates Objects
        public virtual void InstantiateUI(UICharacterEntity prefab)
        {
            if (prefab == null)
                return;
            if (uiCharacterEntity != null)
                Destroy(uiCharacterEntity.gameObject);
            uiCharacterEntity = Instantiate(prefab, CharacterUITransform);
            uiCharacterEntity.transform.localPosition = Vector3.zero;
            uiCharacterEntity.Data = this;
        }
        #endregion

        #region Attack / Receive Damage / Dead / Spawn
        public void ValidateRecovery(BaseCharacterEntity attacker = null)
        {
            if (!IsServer)
                return;

            // Validate Hp
            if (CurrentHp < 0)
                CurrentHp = 0;
            if (CurrentHp > CacheMaxHp)
                CurrentHp = CacheMaxHp;
            // Validate Mp
            if (CurrentMp < 0)
                CurrentMp = 0;
            if (CurrentMp > CacheMaxMp)
                CurrentMp = CacheMaxMp;
            // Validate Stamina
            if (CurrentStamina < 0)
                CurrentStamina = 0;
            if (CurrentStamina > CacheMaxStamina)
                CurrentStamina = CacheMaxStamina;
            // Validate Food
            if (CurrentFood < 0)
                CurrentFood = 0;
            if (CurrentFood > CacheMaxFood)
                CurrentFood = CacheMaxFood;
            // Validate Water
            if (CurrentWater < 0)
                CurrentWater = 0;
            if (CurrentWater > CacheMaxWater)
                CurrentWater = CacheMaxWater;

            if (IsDead())
                Killed(attacker);
        }

        public void GetDamagePositionAndRotation(DamageType damageType, bool isLeftHand, bool hasAimPosition, Vector3 aimPosition, Vector3 stagger, out Vector3 position, out Quaternion rotation)
        {
            if (gameInstance.DimensionType == DimensionType.Dimension2D)
                GetDamagePositionAndRotation2D(damageType, isLeftHand, hasAimPosition, aimPosition, stagger, out position, out rotation);
            else
                GetDamagePositionAndRotation3D(damageType, isLeftHand, hasAimPosition, aimPosition, stagger, out position, out rotation);
        }

        protected void GetDamagePositionAndRotation2D(DamageType damageType, bool isLeftHand, bool hasAimPosition, Vector3 aimPosition, Vector3 stagger, out Vector3 position, out Quaternion rotation)
        {
            position = CacheTransform.position;
            switch (damageType)
            {
                case DamageType.Melee:
                    position = MeleeDamageTransform.position;
                    break;
                case DamageType.Missile:
                    position = MissileDamageTransform.position;
                    break;
            }
            rotation = Quaternion.Euler(0, 0, (Mathf.Atan2(CurrentDirection.y, CurrentDirection.x) * (180 / Mathf.PI)) + 90);
        }

        protected void GetDamagePositionAndRotation3D(DamageType damageType, bool isLeftHand, bool hasAimPosition, Vector3 aimPosition, Vector3 stagger, out Vector3 position, out Quaternion rotation)
        {
            position = CacheTransform.position;
            switch (damageType)
            {
                case DamageType.Melee:
                    position = MeleeDamageTransform.position;
                    break;
                case DamageType.Missile:
                    Transform tempMissileDamageTransform = null;
                    if ((tempMissileDamageTransform = CharacterModel.GetRightHandMissileDamageTransform()) != null && !isLeftHand)
                    {
                        // Use position from right hand weapon missile damage transform
                        position = tempMissileDamageTransform.position;
                    }
                    else if ((tempMissileDamageTransform = CharacterModel.GetLeftHandMissileDamageTransform()) != null && isLeftHand)
                    {
                        // Use position from left hand weapon missile damage transform
                        position = tempMissileDamageTransform.position;
                    }
                    else
                    {
                        // Use position from default missile damage transform
                        position = MissileDamageTransform.position;
                    }
                    break;
            }
            Quaternion forwardRotation = Quaternion.LookRotation(CacheTransform.forward);
            Vector3 forwardStagger = forwardRotation * stagger;
            rotation = Quaternion.LookRotation(CacheTransform.forward + forwardStagger);
            if (hasAimPosition)
            {
                forwardRotation = Quaternion.LookRotation(aimPosition - position);
                forwardStagger = forwardRotation * stagger;
                rotation = Quaternion.LookRotation(aimPosition + forwardStagger - position);
            }
        }

        public override void ReceivedDamage(IAttackerEntity attacker, CombatAmountType combatAmountType, int damage)
        {
            base.ReceivedDamage(attacker, combatAmountType, damage);
            InterruptCastingSkill();
            if (attacker is BaseCharacterEntity)
                gameInstance.GameplayRule.OnCharacterReceivedDamage(attacker as BaseCharacterEntity, this, combatAmountType, damage);
        }

        public virtual void Killed(BaseCharacterEntity lastAttacker)
        {
            StopAllCoroutines();
            buffs.Clear();
            skillUsages.Clear();
            // Send OnDead to owner player only
            RequestOnDead();
        }

        public virtual void Respawn()
        {
            if (!IsServer || !IsDead())
                return;
            CurrentHp = CacheMaxHp;
            CurrentMp = CacheMaxMp;
            CurrentStamina = CacheMaxStamina;
            CurrentFood = CacheMaxFood;
            CurrentWater = CacheMaxWater;
            respawnGroundedCheckCountDown = RESPAWN_GROUNDED_CHECK_DURATION;
            // Send OnRespawn to owner player only
            RequestOnRespawn();
        }

        public void RewardExp(Reward reward, float multiplier, RewardGivenType rewardGivenType)
        {
            if (!IsServer)
                return;
            if (!gameplayRule.RewardExp(this, reward, multiplier, rewardGivenType))
                return;
            // Send OnLevelUp to owner player only
            RequestOnLevelUp();
        }

        public void RewardCurrencies(Reward reward, float multiplier, RewardGivenType rewardGivenType)
        {
            gameplayRule.RewardCurrencies(this, reward, multiplier, rewardGivenType);
        }
        #endregion

        #region Inventory Helpers
        public bool CanEquipItem(CharacterItem equippingItem, InventoryType inventoryType, int oldEquipIndex, out GameMessage.Type gameMessageType, out bool shouldUnequipRightHand, out bool shouldUnequipLeftHand)
        {
            gameMessageType = GameMessage.Type.None;
            shouldUnequipRightHand = false;
            shouldUnequipLeftHand = false;

            Item equipmentItem = equippingItem.GetEquipmentItem();
            if (equipmentItem == null)
            {
                gameMessageType = GameMessage.Type.CannotEquip;
                return false;
            }

            if (!equippingItem.CanEquip(this))
            {
                gameMessageType = GameMessage.Type.LevelOrAttributeNotEnough;
                return false;
            }
            
            EquipWeapons tempEquipWeapons = EquipWeapons;
            Item rightHandWeapon = tempEquipWeapons.rightHand.GetWeaponItem();
            Item leftHandWeapon = tempEquipWeapons.leftHand.GetWeaponItem();
            Item leftHandShield = tempEquipWeapons.leftHand.GetShieldItem();

            WeaponItemEquipType rightHandEquipType;
            bool hasRightHandItem = rightHandWeapon.TryGetWeaponItemEquipType(out rightHandEquipType);
            WeaponItemEquipType leftHandEquipType;
            bool hasLeftHandItem = leftHandShield != null || leftHandWeapon.TryGetWeaponItemEquipType(out leftHandEquipType);

            // Equipping item is weapon
            Item weaponItem = equippingItem.GetWeaponItem();
            if (weaponItem != null)
            {
                switch (weaponItem.EquipType)
                {
                    case WeaponItemEquipType.OneHand:
                        // If weapon is one hand its equip position must be right hand
                        if (inventoryType != InventoryType.EquipWeaponRight)
                        {
                            gameMessageType = GameMessage.Type.InvalidEquipPositionRightHand;
                            return false;
                        }
                        // One hand can equip with shield only 
                        // if there are weapons on left hand it should unequip
                        if (hasRightHandItem)
                            shouldUnequipRightHand = true;
                        if (hasLeftHandItem)
                            shouldUnequipLeftHand = true;
                        break;
                    case WeaponItemEquipType.OneHandCanDual:
                        // If weapon is one hand can dual its equip position must be right or left hand
                        if (inventoryType != InventoryType.EquipWeaponRight &&
                            inventoryType != InventoryType.EquipWeaponLeft)
                        {
                            gameMessageType = GameMessage.Type.InvalidEquipPositionRightHandOrLeftHand;
                            return false;
                        }
                        if (inventoryType == InventoryType.EquipWeaponRight && hasRightHandItem)
                        {
                            shouldUnequipRightHand = true;
                        }
                        if (inventoryType == InventoryType.EquipWeaponLeft && hasLeftHandItem)
                        {
                            shouldUnequipLeftHand = true;
                        }
                        // Unequip item if right hand weapon is one hand or two hand when equipping at left hand
                        if (inventoryType == InventoryType.EquipWeaponLeft && hasRightHandItem)
                        {
                            if (rightHandEquipType == WeaponItemEquipType.OneHand ||
                                rightHandEquipType == WeaponItemEquipType.TwoHand)
                                shouldUnequipRightHand = true;
                        }
                        break;
                    case WeaponItemEquipType.TwoHand:
                        // If weapon is one hand its equip position must be right hand
                        if (inventoryType != InventoryType.EquipWeaponRight)
                        {
                            gameMessageType = GameMessage.Type.InvalidEquipPositionRightHand;
                            return false;
                        }
                        // Unequip both left and right hand
                        if (hasRightHandItem)
                            shouldUnequipRightHand = true;
                        if (hasLeftHandItem)
                            shouldUnequipLeftHand = true;
                        break;
                }
                return true;
            }
            // Equipping item is shield
            Item shieldItem = equippingItem.GetShieldItem();
            if (shieldItem != null)
            {
                if (inventoryType != InventoryType.EquipWeaponLeft)
                {
                    gameMessageType = GameMessage.Type.InvalidEquipPositionLeftHand;
                    return false;
                }
                if (hasRightHandItem && rightHandEquipType == WeaponItemEquipType.TwoHand)
                    shouldUnequipRightHand = true;
                if (hasLeftHandItem)
                    shouldUnequipLeftHand = true;
                return true;
            }
            // Equipping item is armor
            Item armorItem = equippingItem.GetArmorItem();
            if (armorItem != null)
            {
                if (inventoryType != InventoryType.EquipItems)
                {
                    gameMessageType = GameMessage.Type.InvalidEquipPositionArmor;
                    return false;
                }
                if (oldEquipIndex >= 0 && !armorItem.EquipPosition.Equals(EquipItems[oldEquipIndex].GetArmorItem().EquipPosition))
                {
                    gameMessageType = GameMessage.Type.InvalidEquipPositionArmor;
                    return false;
                }
                return true;
            }
            return false;
        }

        public override void ReceiveDamage(IAttackerEntity attacker, CharacterItem weapon, Dictionary<DamageElement, MinMaxFloat> allDamageAmounts, CharacterBuff debuff, uint hitEffectsId)
        {
            if (!IsServer || IsDead() || !CanReceiveDamageFrom(attacker))
                return;

            if (HitBoxes != null && HitBoxes.Length > 0)
                return;

            ReceiveDamageFunction(attacker, weapon, allDamageAmounts, debuff, hitEffectsId);
        }

        internal void ReceiveDamageFunction(IAttackerEntity attacker, CharacterItem weapon, Dictionary<DamageElement, MinMaxFloat> allDamageAmounts, CharacterBuff debuff, uint hitEffectsId)
        {
            base.ReceiveDamage(attacker, weapon, allDamageAmounts, debuff, hitEffectsId);
            BaseCharacterEntity attackerCharacter = attacker as BaseCharacterEntity;

            // Notify enemy spotted when received damage from enemy
            NotifyEnemySpottedToAllies(attackerCharacter);

            // Notify enemy spotted when damage taken to enemy
            attackerCharacter.NotifyEnemySpottedToAllies(this);

            // Calculate chance to hit
            float hitChance = gameInstance.GameplayRule.GetHitChance(attackerCharacter, this);
            // If miss, return don't calculate damages
            if (Random.value > hitChance)
            {
                ReceivedDamage(attackerCharacter, CombatAmountType.Miss, 0);
                return;
            }

            // Calculate damages
            float calculatingTotalDamage = 0f;
            if (allDamageAmounts.Count > 0)
            {
                DamageElement damageElement;
                MinMaxFloat damageAmount;
                float tempReceivingDamage;
                foreach (KeyValuePair<DamageElement, MinMaxFloat> allDamageAmount in allDamageAmounts)
                {
                    damageElement = allDamageAmount.Key;
                    damageAmount = allDamageAmount.Value;
                    // Set hit effect by damage element
                    if (hitEffectsId == 0 && damageElement != gameInstance.DefaultDamageElement)
                        hitEffectsId = damageElement.hitEffects.Id;
                    tempReceivingDamage = damageElement.GetDamageReducedByResistance(this, damageAmount.Random());
                    if (tempReceivingDamage > 0f)
                        calculatingTotalDamage += tempReceivingDamage;
                }
            }

            // Play hit effect
            if (hitEffectsId == 0)
                hitEffectsId = gameInstance.DefaultHitEffects.Id;
            if (hitEffectsId > 0)
                RequestPlayEffect(hitEffectsId);

            // Calculate chance to critical
            float criticalChance = gameInstance.GameplayRule.GetCriticalChance(attackerCharacter, this);
            bool isCritical = Random.value <= criticalChance;
            // If critical occurs
            if (isCritical)
                calculatingTotalDamage = gameInstance.GameplayRule.GetCriticalDamage(attackerCharacter, this, calculatingTotalDamage);

            // Calculate chance to block
            float blockChance = gameInstance.GameplayRule.GetBlockChance(attackerCharacter, this);
            bool isBlocked = Random.value <= blockChance;
            // If block occurs
            if (isBlocked)
                calculatingTotalDamage = gameInstance.GameplayRule.GetBlockDamage(attackerCharacter, this, calculatingTotalDamage);

            // Apply damages
            int totalDamage = (int)calculatingTotalDamage;
            CurrentHp -= totalDamage;

            if (isBlocked)
                ReceivedDamage(attackerCharacter, CombatAmountType.BlockedDamage, totalDamage);
            else if (isCritical)
                ReceivedDamage(attackerCharacter, CombatAmountType.CriticalDamage, totalDamage);
            else
                ReceivedDamage(attackerCharacter, CombatAmountType.NormalDamage, totalDamage);

            if (CharacterModel != null)
                CharacterModel.PlayHitAnimation();

            // If current hp <= 0, character dead
            if (IsDead())
            {
                // Call killed function, this should be called only once when dead
                ValidateRecovery(attackerCharacter);
            }
            else
            {
                // Apply debuff if character is not dead
                if (!debuff.IsEmpty())
                    ApplyBuff(debuff.dataId, debuff.type, debuff.level);
            }
        }
        #endregion

        #region Keys indexes update functions
        protected void UpdateEquipItemIndexes()
        {
            equipItemIndexes.Clear();
            for (int i = 0; i < equipItems.Count; ++i)
            {
                CharacterItem entry = equipItems[i];
                Item armorItem = entry.GetArmorItem();
                if (entry.NotEmptySlot() && armorItem != null && !equipItemIndexes.ContainsKey(armorItem.EquipPosition))
                    equipItemIndexes.Add(armorItem.EquipPosition, i);
            }
        }
        #endregion

        #region Target Entity Getter/Setter
        public virtual void SetTargetEntity(BaseGameEntity entity)
        {
            targetEntity = entity;
        }

        public virtual BaseGameEntity GetTargetEntity()
        {
            return targetEntity;
        }

        public bool TryGetTargetEntity<T>(out T entity) where T : class
        {
            entity = null;
            if (targetEntity == null)
                return false;
            entity = targetEntity as T;
            return entity != null;
        }
        #endregion

        #region Weapons / Damage
        public virtual CrosshairSetting GetCrosshairSetting()
        {
            Item rightHandWeapon = EquipWeapons.rightHand.GetWeaponItem();
            Item leftHandWeapon = EquipWeapons.leftHand.GetWeaponItem();
            if (rightHandWeapon != null && leftHandWeapon != null)
            {
                // Create new crosshair setting based on weapons
                return new CrosshairSetting()
                {
                    hidden = rightHandWeapon.crosshairSetting.hidden || leftHandWeapon.crosshairSetting.hidden,
                    expandPerFrameWhileMoving = (rightHandWeapon.crosshairSetting.expandPerFrameWhileMoving + leftHandWeapon.crosshairSetting.expandPerFrameWhileMoving) / 2f,
                    expandPerFrameWhileAttacking = (rightHandWeapon.crosshairSetting.expandPerFrameWhileAttacking + leftHandWeapon.crosshairSetting.expandPerFrameWhileAttacking) / 2f,
                    shrinkPerFrame = (rightHandWeapon.crosshairSetting.shrinkPerFrame + leftHandWeapon.crosshairSetting.shrinkPerFrame) / 2f,
                    minSpread = (rightHandWeapon.crosshairSetting.minSpread + leftHandWeapon.crosshairSetting.minSpread) / 2f,
                    maxSpread = (rightHandWeapon.crosshairSetting.maxSpread + leftHandWeapon.crosshairSetting.maxSpread) / 2f
                };
            }
            if (rightHandWeapon != null)
                return rightHandWeapon.crosshairSetting;
            if (leftHandWeapon != null)
                return leftHandWeapon.crosshairSetting;
            return gameInstance.DefaultWeaponItem.crosshairSetting;
        }

        public virtual float GetAttackDistance(bool isLeftHand)
        {
            Item rightWeaponItem = EquipWeapons.rightHand.GetWeaponItem();
            Item leftWeaponItem = EquipWeapons.leftHand.GetWeaponItem();
            if (!isLeftHand)
            {
                if (rightWeaponItem != null)
                    return rightWeaponItem.WeaponType.damageInfo.GetDistance();
                if (rightWeaponItem == null && leftWeaponItem != null)
                    return leftWeaponItem.WeaponType.damageInfo.GetDistance();
            }
            else
            {
                if (leftWeaponItem != null)
                    return leftWeaponItem.WeaponType.damageInfo.GetDistance();
                if (leftWeaponItem == null && rightWeaponItem != null)
                    return rightWeaponItem.WeaponType.damageInfo.GetDistance();
            }
            return gameInstance.DefaultWeaponItem.WeaponType.damageInfo.GetDistance();
        }

        public virtual float GetAttackFov(bool isLeftHand)
        {
            Item rightWeaponItem = EquipWeapons.rightHand.GetWeaponItem();
            Item leftWeaponItem = EquipWeapons.leftHand.GetWeaponItem();
            if (!isLeftHand)
            {
                if (rightWeaponItem != null)
                    return rightWeaponItem.WeaponType.damageInfo.GetFov();
                if (rightWeaponItem == null && leftWeaponItem != null)
                    return leftWeaponItem.WeaponType.damageInfo.GetFov();
            }
            else
            {
                if (leftWeaponItem != null)
                    return leftWeaponItem.WeaponType.damageInfo.GetFov();
                if (leftWeaponItem == null && rightWeaponItem != null)
                    return rightWeaponItem.WeaponType.damageInfo.GetFov();
            }
            return gameInstance.DefaultWeaponItem.WeaponType.damageInfo.GetFov();
        }

        public virtual float GetSkillAttackDistance(Skill skill, bool isLeftHand)
        {
            if (skill == null || !skill.IsAttack())
                return 0f;
            if (skill.skillAttackType == SkillAttackType.Normal)
                return skill.damageInfo.GetDistance();
            return GetAttackDistance(isLeftHand);
        }

        public virtual float GetSkillAttackFov(Skill skill, bool isLeftHand)
        {
            if (skill == null || !skill.IsAttack())
                return 0f;
            if (skill.skillAttackType == SkillAttackType.Normal)
                return skill.damageInfo.GetFov();
            return GetAttackFov(isLeftHand);
        }

        public virtual void LaunchDamageEntity(
            bool isLeftHand,
            CharacterItem weapon,
            DamageInfo damageInfo,
            Dictionary<DamageElement, MinMaxFloat> allDamageAmounts,
            CharacterBuff debuff,
            uint hitEffectsId,
            bool hasAimPosition,
            Vector3 aimPosition,
            Vector3 stagger)
        {
            if (!IsServer)
                return;

            IDamageableEntity tempDamageableEntity = null;
            Vector3 damagePosition;
            Quaternion damageRotation;
            GetDamagePositionAndRotation(damageInfo.damageType, isLeftHand, hasAimPosition, aimPosition, stagger, out damagePosition, out damageRotation);
#if UNITY_EDITOR
            debugDamagePosition = damagePosition;
            debugDamageRotation = damageRotation;
#endif
            switch (damageInfo.damageType)
            {
                case DamageType.Melee:
                    if (damageInfo.hitOnlySelectedTarget)
                    {
                        if (!TryGetTargetEntity(out tempDamageableEntity))
                        {
                            int tempOverlapSize = OverlapObjects_ForAttackFunctions(damagePosition, damageInfo.hitDistance, gameInstance.GetDamageableLayerMask());
                            if (tempOverlapSize == 0)
                                return;
                            // Target entity not set, use overlapped object as target
                            for (int tempLoopCounter = 0; tempLoopCounter < tempOverlapSize; ++tempLoopCounter)
                            {
                                tempGameObject = GetOverlapObject_ForAttackFunctions(tempLoopCounter);
                                tempDamageableEntity = tempGameObject.GetComponent<IDamageableEntity>();
                                if (tempDamageableEntity != null && (!(tempDamageableEntity is BaseCharacterEntity) || (BaseCharacterEntity)tempDamageableEntity != this))
                                    break;
                            }
                        }
                        // Target receive damage
                        if (tempDamageableEntity != null && !tempDamageableEntity.IsDead() &&
                            (!(tempDamageableEntity is BaseCharacterEntity) || (BaseCharacterEntity)tempDamageableEntity != this) &&
                            IsPositionInFov(damageInfo.hitFov, tempDamageableEntity.transform.position))
                        {
                            // Pass all receive damage condition, then apply damages
                            tempDamageableEntity.ReceiveDamage(this, weapon, allDamageAmounts, debuff, hitEffectsId);
                        }
                    }
                    else
                    {
                        int tempOverlapSize = OverlapObjects_ForAttackFunctions(damagePosition, damageInfo.hitDistance, gameInstance.GetDamageableLayerMask());
                        if (tempOverlapSize == 0)
                            return;
                        // Find characters that receiving damages
                        for (int tempLoopCounter = 0; tempLoopCounter < tempOverlapSize; ++tempLoopCounter)
                        {
                            tempGameObject = GetOverlapObject_ForAttackFunctions(tempLoopCounter);
                            tempDamageableEntity = tempGameObject.GetComponent<IDamageableEntity>();
                            // Target receive damage
                            if (tempDamageableEntity != null && !tempDamageableEntity.IsDead() &&
                                (!(tempDamageableEntity is BaseCharacterEntity) || (BaseCharacterEntity)tempDamageableEntity != this) &&
                                IsPositionInFov(damageInfo.hitFov, tempDamageableEntity.transform.position))
                            {
                                // Pass all receive damage condition, then apply damages
                                tempDamageableEntity.ReceiveDamage(this, weapon, allDamageAmounts, debuff, hitEffectsId);
                            }
                        }
                    }
                    break;
                case DamageType.Missile:
                    if (damageInfo.missileDamageEntity != null)
                    {
                        GameObject spawnObj = Instantiate(damageInfo.missileDamageEntity.gameObject, damagePosition, damageRotation);
                        MissileDamageEntity missileDamageEntity = spawnObj.GetComponent<MissileDamageEntity>();
                        if (damageInfo.hitOnlySelectedTarget)
                        {
                            if (!TryGetTargetEntity(out tempDamageableEntity))
                                tempDamageableEntity = null;
                        }
                        missileDamageEntity.SetupDamage(this, weapon, allDamageAmounts, debuff, hitEffectsId, damageInfo.missileDistance, damageInfo.missileSpeed, tempDamageableEntity);
                        Manager.Assets.NetworkSpawn(spawnObj);
                    }
                    break;
            }
        }
        #endregion

        #region Allowed abilities
        public virtual bool IsPlayingActionAnimation()
        {
            return animActionType == AnimActionType.AttackRightHand || 
                animActionType == AnimActionType.AttackLeftHand || 
                animActionType == AnimActionType.Skill ||
                animActionType == AnimActionType.ReloadRightHand ||
                animActionType == AnimActionType.ReloadLeftHand;
        }

        public virtual bool CanDoActions()
        {
            return !IsDead() && !IsPlayingActionAnimation() && !isAttackingOrUsingSkill;
        }

        public override sealed float GetMoveSpeed()
        {
            return gameplayRule.GetMoveSpeed(this);
        }

        public override sealed bool CanMove()
        {
            if (IsDead())
                return false;
            if (CacheDisallowMove)
                return false;
            return true;
        }

        public bool CanAttack()
        {
            if (!CanDoActions())
                return false;
            if (CacheDisallowAttack)
                return false;
            if (PassengingVehicleEntity != null &&
                !PassengingVehicleSeat.canAttack)
                return false;
            return true;
        }

        public bool CanUseSkill()
        {
            if (!CanDoActions())
                return false;
            if (CacheDisallowUseSkill)
                return false;
            if (PassengingVehicleEntity != null &&
                !PassengingVehicleSeat.canUseSkill)
                return false;
            return true;
        }

        public bool CanUseItem()
        {
            if (IsDead())
                return false;
            if (CacheDisallowUseItem)
                return false;
            return true;
        }
        #endregion

        #region Find objects helpers
        public int OverlapObjects_ForAttackFunctions(Vector3 position, float distance, int layerMask)
        {
            if (gameInstance.DimensionType == DimensionType.Dimension2D)
                return Physics2D.OverlapCircleNonAlloc(position, distance, overlapColliders2D_ForAttackFunctions, layerMask);
            return Physics.OverlapSphereNonAlloc(position, distance, overlapColliders_ForAttackFunctions, layerMask);
        }

        public GameObject GetOverlapObject_ForAttackFunctions(int index)
        {
            if (gameInstance.DimensionType == DimensionType.Dimension2D)
                return overlapColliders2D_ForAttackFunctions[index].gameObject;
            return overlapColliders_ForAttackFunctions[index].gameObject;
        }

        public int OverlapObjects_ForFindFunctions(Vector3 position, float distance, int layerMask)
        {
            if (gameInstance.DimensionType == DimensionType.Dimension2D)
                return Physics2D.OverlapCircleNonAlloc(position, distance, overlapColliders2D_ForFindFunctions, layerMask);
            return Physics.OverlapSphereNonAlloc(position, distance, overlapColliders_ForFindFunctions, layerMask);
        }

        public GameObject GetOverlapObject_ForFindFunctions(int index)
        {
            if (gameInstance.DimensionType == DimensionType.Dimension2D)
                return overlapColliders2D_ForFindFunctions[index].gameObject;
            return overlapColliders_ForFindFunctions[index].gameObject;
        }

        public bool IsPositionInFov(float fov, Vector3 position)
        {
            return IsPositionInFov(fov, position, CacheTransform.forward);
        }

        public bool IsPositionInFov(float fov, Vector3 position, Vector3 forward)
        {
            if (gameInstance.DimensionType == DimensionType.Dimension2D)
                return IsPositionInFov2D(fov, position, forward);
            return IsPositionInFov3D(fov, position, forward);
        }

        protected bool IsPositionInFov2D(float fov, Vector3 position, Vector3 forward)
        {
            float halfFov = fov * 0.5f;
            Vector2 targetDir = (position - CacheTransform.position).normalized;
            float angle = Vector2.Angle(targetDir, CurrentDirection);
            // Angle in forward position is 180 so we use this value to determine that target is in hit fov or not
            return angle < halfFov;
        }

        protected bool IsPositionInFov3D(float fov, Vector3 position, Vector3 forward)
        {
            float halfFov = fov * 0.5f;
            // This is unsigned angle, so angle found from this function is 0 - 180
            // if position forward from character this value will be 180
            // so just find for angle > 180 - halfFov
            Vector3 targetDir = (position - CacheTransform.position).normalized;
            targetDir.y = 0;
            forward.y = 0;
            targetDir.Normalize();
            forward.Normalize();
            return Vector3.Angle(targetDir, forward) < halfFov;
        }

        public List<T> FindCharacters<T>(float distance, bool findForAliveOnly, bool findForAlly, bool findForEnemy, bool findForNeutral, bool findInFov = false, float fov = 0)
            where T : BaseCharacterEntity
        {
            List<T> result = new List<T>();
            int tempOverlapSize = OverlapObjects_ForFindFunctions(CacheTransform.position, distance, gameInstance.characterLayer.Mask);
            if (tempOverlapSize == 0)
                return null;
            T tempEntity;
            for (int tempLoopCounter = 0; tempLoopCounter < tempOverlapSize; ++tempLoopCounter)
            {
                tempGameObject = GetOverlapObject_ForFindFunctions(tempLoopCounter);
                tempEntity = tempGameObject.GetComponent<T>();
                if (!IsCharacterWhichLookingFor(tempEntity, findForAliveOnly, findForAlly, findForEnemy, findForNeutral, findInFov, fov))
                    continue;
                result.Add(tempEntity);
            }
            return result;
        }

        public List<T> FindAliveCharacters<T>(float distance, bool findForAlly, bool findForEnemy, bool findForNeutral, bool findInFov = false, float fov = 0)
            where T : BaseCharacterEntity
        {
            return FindCharacters<T>(distance, true, findForAlly, findForEnemy, findForNeutral, findInFov, 0);
        }

        public T FindNearestCharacter<T>(float distance, bool findForAliveOnly, bool findForAlly, bool findForEnemy, bool findForNeutral, bool findInFov = false, float fov = 0)
            where T : BaseCharacterEntity
        {
            int tempOverlapSize = OverlapObjects_ForFindFunctions(CacheTransform.position, distance, gameInstance.characterLayer.Mask);
            if (tempOverlapSize == 0)
                return null;
            float tempDistance;
            T tempEntity;
            float nearestDistance = float.MaxValue;
            T nearestEntity = null;
            for (int tempLoopCounter = 0; tempLoopCounter < tempOverlapSize; ++tempLoopCounter)
            {
                tempGameObject = GetOverlapObject_ForFindFunctions(tempLoopCounter);
                tempEntity = tempGameObject.GetComponent<T>();
                if (!IsCharacterWhichLookingFor(tempEntity, findForAliveOnly, findForAlly, findForEnemy, findForNeutral, findInFov, fov))
                    continue;
                tempDistance = Vector3.Distance(CacheTransform.position, tempEntity.CacheTransform.position);
                if (tempDistance < nearestDistance)
                {
                    nearestDistance = tempDistance;
                    nearestEntity = tempEntity;
                }
            }
            return nearestEntity;
        }

        public T FindNearestAliveCharacter<T>(float distance, bool findForAlly, bool findForEnemy, bool findForNeutral, bool findInFov = false, float fov = 0)
            where T : BaseCharacterEntity
        {
            return FindNearestCharacter<T>(distance, true, findForAlly, findForEnemy, findForNeutral, findInFov, fov);
        }

        private bool IsCharacterWhichLookingFor(BaseCharacterEntity characterEntity, bool findForAlive, bool findForAlly, bool findForEnemy, bool findForNeutral, bool findInFov, float fov)
        {
            if (characterEntity == null || characterEntity == this)
                return false;
            if (findForAlive && characterEntity.IsDead())
                return false;
            if (findInFov && !IsPositionInFov(fov, characterEntity.CacheTransform.position))
                return false;
            return (findForAlly && characterEntity.IsAlly(this)) ||
                (findForEnemy && characterEntity.IsEnemy(this)) ||
                (findForNeutral && characterEntity.IsNeutral(this));
        }
        #endregion
        
        private void NotifyEnemySpottedToAllies(BaseCharacterEntity enemy)
        {
            // Warn that this character received damage to nearby characters
            List<BaseCharacterEntity> foundCharacters = FindAliveCharacters<BaseCharacterEntity>(gameInstance.enemySpottedNotifyDistance, true, false, false);
            if (foundCharacters == null || foundCharacters.Count == 0) return;
            foreach (BaseCharacterEntity foundCharacter in foundCharacters)
            {
                foundCharacter.NotifyEnemySpotted(this, enemy);
            }
        }

        public virtual Vector3 GetSummonPosition()
        {
            if (gameInstance.DimensionType == DimensionType.Dimension2D)
                return CacheTransform.position + new Vector3(Random.Range(gameInstance.minSummonDistance, gameInstance.maxSummonDistance) * GenericUtils.GetNegativePositive(), Random.Range(gameInstance.minSummonDistance, gameInstance.maxSummonDistance) * GenericUtils.GetNegativePositive(), 0f);
            return CacheTransform.position + new Vector3(Random.Range(gameInstance.minSummonDistance, gameInstance.maxSummonDistance) * GenericUtils.GetNegativePositive(), 0f, Random.Range(gameInstance.minSummonDistance, gameInstance.maxSummonDistance) * GenericUtils.GetNegativePositive());
        }

        public virtual Quaternion GetSummonRotation()
        {
            if (gameInstance.DimensionType == DimensionType.Dimension2D)
                return Quaternion.identity;
            return CacheTransform.rotation;
        }

        public bool IsNeutral(BaseCharacterEntity characterEntity)
        {
            return !IsAlly(characterEntity) && !IsEnemy(characterEntity);
        }

        public abstract void NotifyEnemySpotted(BaseCharacterEntity ally, BaseCharacterEntity attacker);
        public abstract bool IsAlly(BaseCharacterEntity characterEntity);
        public abstract bool IsEnemy(BaseCharacterEntity characterEntity);
    }
}
