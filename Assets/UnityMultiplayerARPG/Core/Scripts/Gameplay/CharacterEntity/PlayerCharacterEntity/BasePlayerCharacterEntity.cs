﻿using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace MultiplayerARPG
{
    public abstract partial class BasePlayerCharacterEntity : BaseCharacterEntity, IPlayerCharacterData
    {
        [HideInInspector, System.NonSerialized]
        public WarpPortalEntity warpingPortal;
        [HideInInspector, System.NonSerialized]
        public NpcDialog currentNpcDialog;

        [Header("Player Character Settings")]
        [Tooltip("This title will be shown in create scene")]
        public string characterTitle;
        [Tooltip("This is list which used as choice of character classes when create character")]
        public PlayerCharacter[] playerCharacters;
        [Tooltip("Leave this empty to use GameInstance's controller prefab")]
        public BasePlayerCharacterController controllerPrefab;
        

        protected override void EntityAwake()
        {
            base.EntityAwake();
            gameObject.tag = gameInstance.playerTag;
            MigrateDatabase();
        }

        protected override void EntityUpdate()
        {
            base.EntityUpdate();
            if (IsDead())
            {
                StopMove();
                SetTargetEntity(null);
                return;
            }
        }

        protected override void OnValidate()
        {
            base.OnValidate();
#if UNITY_EDITOR
            if (database != null && !(database is PlayerCharacter))
            {
                Debug.LogError("[BasePlayerCharacterEntity] " + name + " Database must be `PlayerCharacter`");
                database = null;
                EditorUtility.SetDirty(this);
            }
            if (MigrateDatabase())
                EditorUtility.SetDirty(this);
#endif
        }

        private bool MigrateDatabase()
        {
            bool hasChanges = false;
            if (database != null && database is PlayerCharacter)
            {
                List<PlayerCharacter> list = playerCharacters == null ? new List<PlayerCharacter>() : new List<PlayerCharacter>(playerCharacters);
                list.Add(database as PlayerCharacter);
                playerCharacters = list.ToArray();
                database = null;
                hasChanges = true;
            }
            return hasChanges;
        }

        protected override void ApplySkill(Skill skill, short level, bool isLeftHand, CharacterItem weapon, DamageInfo damageInfo, Dictionary<DamageElement, MinMaxFloat> allDamageAmounts, bool hasAimPosition, Vector3 aimPosition)
        {
            base.ApplySkill(skill, level, isLeftHand, weapon, damageInfo, allDamageAmounts, hasAimPosition, aimPosition);
            
            switch (skill.skillType)
            {
                case SkillType.CraftItem:
                    GameMessage.Type gameMessageType;
                    if (!skill.itemCraft.CanCraft(this, out gameMessageType))
                        gameManager.SendServerGameMessage(ConnectionId, gameMessageType);
                    else
                        skill.itemCraft.CraftItem(this);
                    break;
            }
        }

        public override void Respawn()
        {
            if (!IsServer || !IsDead())
                return;
            base.Respawn();
            gameManager.RespawnCharacter(this);
        }

        public override bool CanReceiveDamageFrom(IAttackerEntity attacker)
        {
            if (attacker == null)
                return false;

            BaseCharacterEntity characterEntity = attacker as BaseCharacterEntity;
            if (characterEntity == null)
                return false;

            if (isInSafeArea || characterEntity.isInSafeArea)
            {
                // If this character or another character is in safe area so it cannot receive damage
                return false;
            }
            if (characterEntity is BasePlayerCharacterEntity)
            {
                // If not ally while this is Pvp map, assume that it can receive damage
                if (!IsAlly(characterEntity) && gameManager.CurrentMapInfo.canPvp)
                    return true;
            }
            if (characterEntity is BaseMonsterCharacterEntity)
            {
                // If this character is not summoner so it is enemy and also can receive damage
                return !IsAlly(characterEntity);
            }
            return false;
        }

        public override bool IsAlly(BaseCharacterEntity characterEntity)
        {
            if (characterEntity == null)
                return false;

            if (characterEntity is BasePlayerCharacterEntity)
            {
                // If this character is in same party or guild with another character so it is ally
                BasePlayerCharacterEntity playerCharacterEntity = characterEntity as BasePlayerCharacterEntity;
                return (PartyId > 0 && PartyId == playerCharacterEntity.PartyId) ||
                    (GuildId > 0 && GuildId == playerCharacterEntity.GuildId);
            }
            if (characterEntity is BaseMonsterCharacterEntity)
            {
                // If this character is summoner so it is ally
                BaseMonsterCharacterEntity monsterCharacterEntity = characterEntity as BaseMonsterCharacterEntity;
                return monsterCharacterEntity.Summoner != null && monsterCharacterEntity.Summoner == this;
            }
            return false;
        }

        public override bool IsEnemy(BaseCharacterEntity characterEntity)
        {
            if (characterEntity == null)
                return false;

            if (characterEntity is BasePlayerCharacterEntity)
            {
                // If not ally while this is Pvp map, assume that it is enemy while both characters are not in safe zone
                if (!IsAlly(characterEntity) && gameManager.CurrentMapInfo.canPvp)
                    return !isInSafeArea && !characterEntity.isInSafeArea;
            }
            if (characterEntity is BaseMonsterCharacterEntity)
            {
                // If this character is not summoner so it is enemy
                BaseMonsterCharacterEntity monsterCharacterEntity = characterEntity as BaseMonsterCharacterEntity;
                return monsterCharacterEntity.Summoner == null || monsterCharacterEntity.Summoner != this;
            }
            return false;
        }

        public override sealed void Killed(BaseCharacterEntity lastAttacker)
        {
            float expLostPercentage = gameInstance.GameplayRule.GetExpLostPercentageWhenDeath(this);
            GuildData guildData;
            if (gameManager.TryGetGuild(GuildId, out guildData))
                expLostPercentage -= expLostPercentage * guildData.DecreaseExpLostPercentage;
            if (expLostPercentage <= 0f)
                expLostPercentage = 0f;
            int exp = Exp;
            exp -= (int)(this.GetNextLevelExp() * expLostPercentage / 100f);
            if (exp <= 0)
                exp = 0;
            Exp = exp;

            base.Killed(lastAttacker);
            currentNpcDialog = null;
        }

        public virtual void OnKillMonster(BaseMonsterCharacterEntity monsterCharacterEntity)
        {
            if (!IsServer || monsterCharacterEntity == null)
                return;

            for (int i = 0; i < Quests.Count; ++i)
            {
                CharacterQuest quest = Quests[i];
                if (quest.AddKillMonster(monsterCharacterEntity, 1))
                    quests[i] = quest;
            }
        }

        public virtual void ExchangeDealingItemsAndGold()
        {
            if (DealingCharacter == null)
                return;
            List<DealingCharacterItem> tempDealingItems = new List<DealingCharacterItem>(DealingItems);
            for (int i = nonEquipItems.Count - 1; i >= 0; --i)
            {
                CharacterItem nonEquipItem = nonEquipItems[i];
                for (int j = tempDealingItems.Count - 1; j >= 0; --j)
                {
                    DealingCharacterItem dealingItem = tempDealingItems[j];
                    if (dealingItem.nonEquipIndex == i && nonEquipItem.amount >= dealingItem.characterItem.amount)
                    {
                        if (DealingCharacter.IncreaseItems(dealingItem.characterItem))
                        {
                            // Reduce item amount when able to increase item to co character
                            nonEquipItem.amount -= dealingItem.characterItem.amount;
                            if (nonEquipItem.amount == 0)
                                nonEquipItems.RemoveAt(i);
                            else
                                nonEquipItems[i] = nonEquipItem;
                        }
                        tempDealingItems.RemoveAt(j);
                        break;
                    }
                }
            }
            this.FillEmptySlots();
            Gold -= DealingGold;
            DealingCharacter.Gold += DealingGold;
        }

        public virtual void ClearDealingData()
        {
            DealingState = DealingState.None;
            DealingGold = 0;
            DealingItems.Clear();
        }

        public override bool CanDoActions()
        {
            return base.CanDoActions() && DealingState == DealingState.None;
        }

        public override void NotifyEnemySpotted(BaseCharacterEntity ally, BaseCharacterEntity attacker)
        {
            // TODO: May send data to client
        }
    }
}