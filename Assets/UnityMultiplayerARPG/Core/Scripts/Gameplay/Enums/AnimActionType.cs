﻿namespace MultiplayerARPG
{
    public enum AnimActionType : byte
    {
        None,
        Generic,
        AttackRightHand,
        AttackLeftHand,
        Skill,
        ReloadRightHand,
        ReloadLeftHand,
    }
}
