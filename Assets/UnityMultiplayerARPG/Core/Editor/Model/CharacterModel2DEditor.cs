﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;

namespace MultiplayerARPG
{
    [CustomEditor(typeof(CharacterModel2D))]
    [CanEditMultipleObjects]
    public class CharacterModel2DEditor : BaseCharacterModelEditor
    {
    }
}
