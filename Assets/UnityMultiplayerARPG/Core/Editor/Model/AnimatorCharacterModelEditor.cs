﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;

namespace MultiplayerARPG
{
    [CustomEditor(typeof(AnimatorCharacterModel))]
    [CanEditMultipleObjects]
    public class AnimatorCharacterModelEditor : BaseCharacterModelEditor
    {
    }
}
