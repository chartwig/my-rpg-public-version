﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DungeonLiege.Core;
using System.Linq;

namespace DungeonLiege.Tests
{
    public class MockFileIO<DT> : IFileIO
    {
        private bool DoesExist = false;
        private string ModulePath = string.Empty;
        private DT LoadObject;
        private List<DT> LoadObjects;
        private string ReadText = string.Empty;
        //Shouldn't need this but something isn't right about the enumerators in a unit test
        private int LoadObjectsIndex = 0;

        private int WasGetModulePathCalled = 0;
        private int WasSaveToConfigCalled = 0;
        private int WasWriteAllTextCalled = 0;

        public bool IsList = false;

        public int VerifyGetModulePathCalled()
        {
            return WasGetModulePathCalled;
        }

        public int VerifySaveToConfigCalled()
        {
            return WasSaveToConfigCalled;
        }

        public int VerifyWriteAllTextCalled()
        {
            return WasWriteAllTextCalled;
        }

        public void MockExists(bool doesExist)
        {
            DoesExist = doesExist;
        }

        public void MockGetModulePath(string modulePath)
        {
            WasGetModulePathCalled++;

            ModulePath = modulePath;
        }

        public void MockLoadFromConfig<T>(T loadObject) where T : DT
        {
            LoadObject = loadObject;
        }
        public void MockLoadFromConfig<T>(List<T> loadObjects) where T : DT
        {
            IsList = true;

            LoadObjectsIndex = 0;

            LoadObjects = loadObjects.Cast<DT>().ToList<DT>();
        }
        public void MockReadAllText(string readText)
        {
            ReadText = readText;
        }

        public bool Exists(string filePath)
        {
            return DoesExist;
        }

        public string GetModulePath()
        {
            return ModulePath;
        }

        public T LoadFromConfig<T>(string configPath)
        {
            T returnValue;

            if (IsList && LoadObjects.Count >= LoadObjectsIndex + 1)
            {
                returnValue = LoadObjects.Cast<T>().ToList<T>()[LoadObjectsIndex];
                LoadObjectsIndex++;
            }
            else
            {
                returnValue = (T)(object)LoadObject;
            }
            return returnValue;
        }

        public string ReadAllText(string path)
        {
            return ReadText;
        }

        public void SaveToConfig<T>(string configPath, T source)
        {
            WasSaveToConfigCalled++;
        }

        public void WriteAllText(string path, string text)
        {
            WasWriteAllTextCalled++;
        }
    }
}
