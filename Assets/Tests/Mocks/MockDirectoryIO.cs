﻿using System.Collections.Generic;
using DungeonLiege.Core;

namespace DungeonLiege.Tests
{
    public class MockDirectoryIO : IDirectoryIO
    {
        private bool DoesExist = false;
        private IEnumerable<string> Directories;

        private int WasExistsCalled = 0;
        private int WasCreateCalled = 0;

        public void MockExists(bool doesExist)
        {
            DoesExist = doesExist;
        }

        public void MockGetDirectories(IEnumerable<string> directories)
        {
            Directories = directories;
        }

        public int VerifyExistsCalled()
        {
            return WasExistsCalled;
        }

        public int VerifyCreateCalled()
        {
            return WasCreateCalled;
        }

        public void Create(string directoryPath)
        {
            WasCreateCalled++;
        }

        public IEnumerable<string> EnumerateFiles(string directoryPath)
        {
            throw new System.NotImplementedException();
        }

        public bool Exists(string directoryPath)
        {
            WasExistsCalled++;

            return DoesExist;
        }

        public int FileCount(string directoryPath)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<string> GetDirectories(string path)
        {
            return Directories;
        }
    }
}
