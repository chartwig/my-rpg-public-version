﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DungeonLiege.Core;
using DungeonLiege.Entities;
using System;

namespace DungeonLiege.Tests
{
    public class MockBoardManager : IBoardManager
    {
        private Board Board { get; set; }
        
        private int WasCreateCalled { get; set; }

        public void MockCreate(Board board)
        {
            Board = board;
        }

        public Board Create(string boardName, Dictionary<Guid, GameObject> sceneEntities)
        {
            return Board;
        }
    }
}
