﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Zenject;
using DungeonLiege.Core;
using DungeonLiege.Entities;

namespace DungeonLiege.Tests
{
    [TestFixture]
    public class CampaignMetaManagerTests : ZenjectUnitTestFixture
    {
        [SetUp]
        public void CommonInstalls()
        {
            // Our Mocks, as Singletons, so that the test can modify values,
            // and then have those values in our actual run
            Container.Bind<IFileIO>().To<MockFileIO<CampaignMeta>>().AsSingle();
            Container.Bind<IDirectoryIO>().To<MockDirectoryIO>().AsSingle();
        }

        [TearDown]
        public void CommonUninstalls()
        {
            Container.UnbindAll();
        }

        [UnityTest]
        public IEnumerator SaveTest()
        {
            // Arrange
            MockFileIO<CampaignMeta> mockFileIO = Container.Resolve<IFileIO>() as MockFileIO<CampaignMeta>;
            mockFileIO.MockGetModulePath("TestPath");

            MockDirectoryIO mockDirectoryIO = Container.Resolve<IDirectoryIO>() as MockDirectoryIO;
            mockDirectoryIO.MockExists(true);

            CampaignMetaManager campaignMetaManager = new CampaignMetaManager();

            CampaignMeta campaignMeta = new CampaignMeta() {
                AuthorName = "Test Author Name",
                Description = "Test Description",
                Name = "Test Name"
            };

            Container.Inject(campaignMetaManager);

            // Act
            campaignMetaManager.Save(campaignMeta);

            yield return new WaitForSeconds(0.1f);

            // Assert
            Assert.IsTrue(mockFileIO.VerifyGetModulePathCalled() > 0);
            Assert.IsTrue(mockDirectoryIO.VerifyExistsCalled() > 0);
            Assert.IsTrue(mockFileIO.VerifySaveToConfigCalled() > 0);

            yield break;
        }

        [UnityTest]
        public IEnumerator LoadTest()
        {
            // Arrange
            CampaignMeta actualMeta;

            CampaignMeta expectedMeta = new CampaignMeta()
            {
                AuthorName = "Test Author Name",
                Description = "Test Description",
                Name = "Test Name"
            };

            MockFileIO<CampaignMeta> mockFileIO = Container.Resolve<IFileIO>() as MockFileIO<CampaignMeta>;
            mockFileIO.MockGetModulePath("TestPath");
            // Could reuse exceptedMeta, but this ensures that they are equal, rather than "equal by coincedence"
            mockFileIO.MockLoadFromConfig<CampaignMeta>(new CampaignMeta()
            {
                AuthorName = "Test Author Name",
                Description = "Test Description",
                Name = "Test Name"
            });

            MockDirectoryIO mockDirectoryIO = Container.Resolve<IDirectoryIO>() as MockDirectoryIO;
            mockDirectoryIO.MockExists(true);

            CampaignMetaManager campaignMetaManager = new CampaignMetaManager();

            Container.Inject(campaignMetaManager);

            // Act
            actualMeta = campaignMetaManager.Load("Test Name");

            yield return new WaitForSeconds(0.1f);

            // Assert
            Assert.IsTrue(mockFileIO.VerifyGetModulePathCalled() > 0);
            Assert.IsTrue(mockDirectoryIO.VerifyExistsCalled() > 0);
            Assert.AreEqual(expectedMeta, actualMeta);

            yield break;
        }

        [UnityTest]
        public IEnumerator LoadAllTest()
        {
            // Assert
            List<CampaignMeta> actualMetas;

            List<CampaignMeta> expectedMetas = new List<CampaignMeta>() {
                new CampaignMeta()
                {
                    AuthorName = "Test Author Name",
                    Description = "Test Description",
                    Name = "Test Name"
                },
                new CampaignMeta()
                {
                    AuthorName = "Second Test",
                    Description = "Second Description",
                    Name = "Second Name"
                }
            };

            MockFileIO<CampaignMeta> mockFileIO = Container.Resolve<IFileIO>() as MockFileIO<CampaignMeta>;
            mockFileIO.MockGetModulePath("TestPath");
            mockFileIO.MockExists(true);
            // Could reuse exceptedMeta, but this ensures that they are equal, rather than "equal by coincedence"
            mockFileIO.MockLoadFromConfig<CampaignMeta>(new List<CampaignMeta>() {
                new CampaignMeta()
                {
                    AuthorName = "Test Author Name",
                    Description = "Test Description",
                    Name = "Test Name"
                },
                new CampaignMeta()
                {
                    AuthorName = "Second Test",
                    Description = "Second Description",
                    Name = "Second Name"
                }
            });

            MockDirectoryIO mockDirectoryIO = Container.Resolve<IDirectoryIO>() as MockDirectoryIO;
            mockDirectoryIO.MockExists(true);
            mockDirectoryIO.MockGetDirectories(new List<string>() { "Directory", "Directory2" });

            CampaignMetaManager campaignMetaManager = new CampaignMetaManager();

            Container.Inject(campaignMetaManager);

            // Act
            actualMetas = campaignMetaManager.LoadAll();

            yield return new WaitForSeconds(0.1f);

            // Assert
            Assert.IsTrue(mockFileIO.VerifyGetModulePathCalled() > 0);
            Assert.IsTrue(mockDirectoryIO.VerifyExistsCalled() > 0);
            Assert.AreEqual(expectedMetas, actualMetas);

            yield break;
        }

        [UnityTest]
        public IEnumerator LoadAllSingleDirectoryTest()
        {
            // Assert
            List<CampaignMeta> actualMetas;

            List<CampaignMeta> expectedMetas = new List<CampaignMeta>() {
                new CampaignMeta()
                {
                    AuthorName = "Test Author Name",
                    Description = "Test Description",
                    Name = "Test Name"
                }
            };

            MockFileIO<CampaignMeta> mockFileIO = Container.Resolve<IFileIO>() as MockFileIO<CampaignMeta>;
            mockFileIO.MockGetModulePath("TestPath");
            mockFileIO.MockExists(true);
            // Could reuse exceptedMeta, but this ensures that they are equal, rather than "equal by coincedence"
            mockFileIO.MockLoadFromConfig<CampaignMeta>(new List<CampaignMeta>() {
                new CampaignMeta()
                {
                    AuthorName = "Test Author Name",
                    Description = "Test Description",
                    Name = "Test Name"
                }
            });

            MockDirectoryIO mockDirectoryIO = Container.Resolve<IDirectoryIO>() as MockDirectoryIO;
            mockDirectoryIO.MockExists(true);
            mockDirectoryIO.MockGetDirectories(new List<string>() { "Directory" });

            CampaignMetaManager campaignMetaManager = new CampaignMetaManager();

            Container.Inject(campaignMetaManager);

            // Act
            actualMetas = campaignMetaManager.LoadAll();

            yield return new WaitForSeconds(0.1f);

            // Assert
            Assert.IsTrue(mockFileIO.VerifyGetModulePathCalled() > 0);
            Assert.AreEqual(expectedMetas, actualMetas);

            yield break;
        }

        [UnityTest]
        public IEnumerator LoadAllSingleDirectoryCreatesModuleDirectoryTest()
        {
            // Assert
            List<CampaignMeta> actualMetas;

            MockFileIO<CampaignMeta> mockFileIO = Container.Resolve<IFileIO>() as MockFileIO<CampaignMeta>;
            mockFileIO.MockGetModulePath("TestPath");
            mockFileIO.MockExists(false);
            // Could reuse exceptedMeta, but this ensures that they are equal, rather than "equal by coincedence"
            mockFileIO.MockLoadFromConfig<CampaignMeta>(new List<CampaignMeta>() { });

            MockDirectoryIO mockDirectoryIO = Container.Resolve<IDirectoryIO>() as MockDirectoryIO;
            mockDirectoryIO.MockExists(false);
            mockDirectoryIO.MockGetDirectories(new List<string>() { "Directory" });

            CampaignMetaManager campaignMetaManager = new CampaignMetaManager();

            Container.Inject(campaignMetaManager);

            // Act
            actualMetas = campaignMetaManager.LoadAll();

            yield return new WaitForSeconds(0.1f);

            // Assert
            Assert.IsTrue(mockFileIO.VerifyGetModulePathCalled() > 0);
            Assert.IsTrue(mockDirectoryIO.VerifyExistsCalled() > 0);
            Assert.IsTrue(mockDirectoryIO.VerifyCreateCalled() > 0);
            Assert.IsNotNull(actualMetas);
            Assert.IsEmpty(actualMetas);

            yield break;
        }

        [UnityTest]
        public IEnumerator SaveCreatesDirectoriesTest()
        {
            // Arrange
            MockFileIO<CampaignMeta> mockFileIO = Container.Resolve<IFileIO>() as MockFileIO<CampaignMeta>;
            mockFileIO.MockGetModulePath("TestPath");

            MockDirectoryIO mockDirectoryIO = Container.Resolve<IDirectoryIO>() as MockDirectoryIO;
            mockDirectoryIO.MockExists(false);

            CampaignMetaManager campaignMetaManager = new CampaignMetaManager();

            CampaignMeta campaignMeta = new CampaignMeta() {
                AuthorName = "Test Author Name",
                Description = "Test Description",
                Name = "Test Name"
            };

            Container.Inject(campaignMetaManager);

            // Act
            campaignMetaManager.Save(campaignMeta);

            yield return new WaitForSeconds(0.1f);

            // Assert
            Assert.IsTrue(mockFileIO.VerifyGetModulePathCalled() > 0);
            Assert.AreEqual(mockDirectoryIO.VerifyExistsCalled(), 2);
            Assert.IsTrue(mockFileIO.VerifySaveToConfigCalled() > 0);

            yield break;
        }

        [UnityTest]
        public IEnumerator LoadCreatesDirectoriesTest()
        {
            // Arrange
            CampaignMeta actualMeta;

            CampaignMeta expectedMeta = new CampaignMeta()
            {
                AuthorName = "Test Author Name",
                Description = "Test Description",
                Name = "Test Name"
            };

            MockFileIO<CampaignMeta> mockFileIO = Container.Resolve<IFileIO>() as MockFileIO<CampaignMeta>;
            mockFileIO.MockGetModulePath("TestPath");
            // Could reuse exceptedMeta, but this ensures that they are equal, rather than "equal by coincedence"
            mockFileIO.MockLoadFromConfig<CampaignMeta>(new CampaignMeta()
            {
                AuthorName = "Test Author Name",
                Description = "Test Description",
                Name = "Test Name"
            });

            MockDirectoryIO mockDirectoryIO = Container.Resolve<IDirectoryIO>() as MockDirectoryIO;
            mockDirectoryIO.MockExists(true);

            CampaignMetaManager campaignMetaManager = new CampaignMetaManager();

            Container.Inject(campaignMetaManager);

            // Act
            actualMeta = campaignMetaManager.Load("Test Name");

            yield return new WaitForSeconds(0.1f);

            // Assert
            Assert.IsTrue(mockFileIO.VerifyGetModulePathCalled() > 0);
            Assert.AreEqual(mockDirectoryIO.VerifyExistsCalled(), 2);
            Assert.AreEqual(expectedMeta, actualMeta);

            yield break;
        }

    }
}
