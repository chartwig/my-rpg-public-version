﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DungeonLiege.Core;
using DungeonLiege.Entities;
using DungeonLiege.UI;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace DungeonLiege.Tests
{
    public class BoardManagerTests
    {
        [UnityTest]
        public IEnumerator CreateTest()
        {
            // Arrange
            GameObject sceneEntity = new GameObject("Test");
            sceneEntity.transform.position = new Vector3(1.0f, 1.0f, 1.0f);
            sceneEntity.transform.rotation = Quaternion.identity;

            EntityProperties entityProps = sceneEntity.AddComponent<EntityProperties>();
            entityProps.AssetName = "TestAsset";
            entityProps.EntityId = System.Guid.Parse("01010101-0101-0101-0101-010101010101");

            Board expectedBoard = new Board() {
                Name = "TestBoard",
                Entities = new Dictionary<System.Guid, Entity>()
                {
                    {
                        System.Guid.Parse("01010101-0101-0101-0101-010101010101"),
                        new Entity()
                        {
                            AssetName = "TestAsset",
                            Position = new Position(new Vector3(1.0f, 1.0f, 1.0f)),
                            Rotation = new Rotation(Quaternion.identity)
                        }
                    }
                }
            };

            Board actualBoard;

            Dictionary<System.Guid, GameObject> sceneEntities = new Dictionary<System.Guid, GameObject>() {
                {
                    System.Guid.Parse("01010101-0101-0101-0101-010101010101"),
                    sceneEntity
                }
            };

            BoardManager boardManager = new BoardManager();

            // Act
            actualBoard = boardManager.Create("TestBoard", sceneEntities);

            yield return new WaitForSeconds(0.1f);

            // Assert
            Assert.AreEqual(expectedBoard.Name, actualBoard.Name);
            Assert.IsTrue(expectedBoard.Entities.Count > 0);
            Assert.AreEqual(expectedBoard.Entities.Count, actualBoard.Entities.Count);

            Entity expectedEntity = expectedBoard.Entities.Values.ToList<Entity>()[0];
            Entity actualEntity = actualBoard.Entities.Values.ToList<Entity>()[0];
            Assert.AreEqual(expectedEntity.AssetName, actualEntity.AssetName);
            Assert.AreEqual(expectedEntity.Position, actualEntity.Position);
            Assert.AreEqual(expectedEntity.Rotation, actualEntity.Rotation);

            yield break;
        }
    }
}
