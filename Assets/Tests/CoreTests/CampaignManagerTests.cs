﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Zenject;
using DungeonLiege.Core;
using DungeonLiege.Entities;

namespace DungeonLiege.Tests
{
    [TestFixture]
    public class CampaignManagerTests : ZenjectUnitTestFixture
    {
        [SetUp]
        public void CommonInstalls()
        {
            // Our Mocks, as Singletons, so that the test can modify values,
            // and then have those values in our actual run
            Container.Bind<IFileIO>().To<MockFileIO<CampaignData>>().AsSingle();
            Container.Bind<IDirectoryIO>().To<MockDirectoryIO>().AsSingle();
            Container.Bind<IBoardManager>().To<MockBoardManager>().AsSingle();
        }

        [TearDown]
        public void CommonUninstalls()
        {
            Container.UnbindAll();
        }

        [UnityTest]
        public IEnumerator SaveTest()
        {
            // Arrange
            MockFileIO<CampaignData> mockFileIO = Container.Resolve<IFileIO>() as MockFileIO<CampaignData>;
            mockFileIO.MockGetModulePath("TestPath");

            MockDirectoryIO mockDirectoryIO = Container.Resolve<IDirectoryIO>() as MockDirectoryIO;
            mockDirectoryIO.MockExists(true);

            MockBoardManager mockBoardManager = Container.Resolve<IBoardManager>() as MockBoardManager;
            mockBoardManager.MockCreate(new Board()
            {
                Name = "TestBoard",
                Entities = new Dictionary<System.Guid, Entity>()
                {
                    {
                        System.Guid.NewGuid(),
                        new Entity()
                        {
                            AssetName = "TestAsset",
                            Position = new Position(new Vector3(0, 0, 0)),
                            Rotation = new Rotation(new Quaternion(0, 0, 0, 0))
                        }
                    }
                }
            });

            CampaignManager campaignManager = new CampaignManager();

            Container.Inject(campaignManager);

            CampaignData campaignData = new CampaignData()
            {
                Boards = new Dictionary<string, Board>()
            };

            // Act

            campaignManager.Save("TestCampaign", "TestBoard", new Dictionary<System.Guid, GameObject>(), campaignData);

            yield return new WaitForSeconds(0.1f);

            // Assert

            Assert.IsTrue(mockFileIO.VerifyGetModulePathCalled() > 0);
            Assert.IsTrue(mockDirectoryIO.VerifyExistsCalled() == 2);
            Assert.IsTrue(mockFileIO.VerifySaveToConfigCalled() > 0);

            yield break;

        }

        [UnityTest]
        public IEnumerator LoadTest()
        {
            // Arrange
            CampaignData actualCampaignData;

            CampaignData expectedCampaignData = new CampaignData()
            {
                Boards = new Dictionary<string, Board>()
                {
                    {
                        "TestBoard",
                        new Board() {
                            Name = "TestBoard",
                            Entities = new Dictionary<System.Guid, Entity>()
                            {
                                {
                                    System.Guid.NewGuid(),
                                    new Entity()
                                    {
                                        AssetName = "TestAsset",
                                        Position = new Position(new Vector3(0, 0, 0)),
                                        Rotation = new Rotation(new Quaternion(0, 0, 0, 0))
                                    }
                                }
                            }
                        }
                    }
                }
            };

            MockFileIO<CampaignData> mockFileIO = Container.Resolve<IFileIO>() as MockFileIO<CampaignData>;
            mockFileIO.MockGetModulePath("TestPath");
            mockFileIO.MockLoadFromConfig<CampaignData>(new CampaignData()
            {
                Boards = new Dictionary<string, Board>()
                {
                    {
                        "TestBoard",
                        new Board() {
                            Name = "TestBoard",
                            Entities = new Dictionary<System.Guid, Entity>()
                            {
                                {
                                    System.Guid.NewGuid(),
                                    new Entity()
                                    {
                                        AssetName = "TestAsset",
                                        Position = new Position(new Vector3(0, 0, 0)),
                                        Rotation = new Rotation(new Quaternion(0, 0, 0, 0))
                                    }
                                }
                            }
                        }
                    }
                }
            });

            MockDirectoryIO mockDirectoryIO = Container.Resolve<IDirectoryIO>() as MockDirectoryIO;
            mockDirectoryIO.MockExists(true);

            CampaignManager campaignManager = new CampaignManager();

            Container.Inject(campaignManager);


            // Act
            actualCampaignData = campaignManager.Load("TestCampaign");

            yield return new WaitForSeconds(0.1f);

            // Assert

            Assert.IsTrue(mockFileIO.VerifyGetModulePathCalled() > 0);
            Assert.IsTrue(mockDirectoryIO.VerifyExistsCalled() == 2);
            Assert.AreEqual(expectedCampaignData.Boards.Count, actualCampaignData.Boards.Count);

            yield break;
        }

        [UnityTest]
        public IEnumerator SaveCreatesDirectoryTest()
        {
            // Arrange
            MockFileIO<CampaignData> mockFileIO = Container.Resolve<IFileIO>() as MockFileIO<CampaignData>;
            mockFileIO.MockGetModulePath("TestPath");

            MockDirectoryIO mockDirectoryIO = Container.Resolve<IDirectoryIO>() as MockDirectoryIO;
            mockDirectoryIO.MockExists(false);

            MockBoardManager mockBoardManager = Container.Resolve<IBoardManager>() as MockBoardManager;
            mockBoardManager.MockCreate(new Board()
            {
                Name = "TestBoard",
                Entities = new Dictionary<System.Guid, Entity>()
                {
                    {
                        System.Guid.NewGuid(),
                        new Entity()
                        {
                            AssetName = "TestAsset",
                            Position = new Position(new Vector3(0, 0, 0)),
                            Rotation = new Rotation(new Quaternion(0, 0, 0, 0))
                        }
                    }
                }
            });

            CampaignManager campaignManager = new CampaignManager();

            Container.Inject(campaignManager);

            CampaignData campaignData = new CampaignData()
            {
                Boards = new Dictionary<string, Board>()
            };

            // Act

            campaignManager.Save("TestCampaign", "TestBoard", new Dictionary<System.Guid, GameObject>(), campaignData);

            yield return new WaitForSeconds(0.1f);

            // Assert

            Assert.IsTrue(mockFileIO.VerifyGetModulePathCalled() > 0);
            Assert.IsTrue(mockDirectoryIO.VerifyExistsCalled() == 2);
            Assert.IsTrue(mockDirectoryIO.VerifyCreateCalled() > 0);
            Assert.IsTrue(mockFileIO.VerifySaveToConfigCalled() > 0);

            yield break;

        }

        [UnityTest]
        public IEnumerator LoadCreatesDirectoryTest()
        {
            // Arrange
            CampaignData actualCampaignData;

            CampaignData expectedCampaignData = new CampaignData()
            {
                Boards = new Dictionary<string, Board>()
                {
                    {
                        "TestBoard",
                        new Board() {
                            Name = "TestBoard",
                            Entities = new Dictionary<System.Guid, Entity>()
                            {
                                {
                                    System.Guid.NewGuid(),
                                    new Entity()
                                    {
                                        AssetName = "TestAsset",
                                        Position = new Position(new Vector3(0, 0, 0)),
                                        Rotation = new Rotation(new Quaternion(0, 0, 0, 0))
                                    }
                                }
                            }
                        }
                    }
                }
            };

            MockFileIO<CampaignData> mockFileIO = Container.Resolve<IFileIO>() as MockFileIO<CampaignData>;
            mockFileIO.MockGetModulePath("TestPath");
            mockFileIO.MockLoadFromConfig<CampaignData>(new CampaignData()
            {
                Boards = new Dictionary<string, Board>()
                {
                    {
                        "TestBoard",
                        new Board() {
                            Name = "TestBoard",
                            Entities = new Dictionary<System.Guid, Entity>()
                            {
                                {
                                    System.Guid.NewGuid(),
                                    new Entity()
                                    {
                                        AssetName = "TestAsset",
                                        Position = new Position(new Vector3(0, 0, 0)),
                                        Rotation = new Rotation(new Quaternion(0, 0, 0, 0))
                                    }
                                }
                            }
                        }
                    }
                }
            });

            MockDirectoryIO mockDirectoryIO = Container.Resolve<IDirectoryIO>() as MockDirectoryIO;
            mockDirectoryIO.MockExists(false);

            CampaignManager campaignManager = new CampaignManager();

            Container.Inject(campaignManager);


            // Act
            actualCampaignData = campaignManager.Load("TestCampaign");

            yield return new WaitForSeconds(0.1f);

            // Assert

            Assert.IsTrue(mockFileIO.VerifyGetModulePathCalled() > 0);
            Assert.IsTrue(mockDirectoryIO.VerifyExistsCalled() == 2);
            Assert.IsTrue(mockDirectoryIO.VerifyCreateCalled() > 0);
            Assert.AreEqual(expectedCampaignData.Boards.Count, actualCampaignData.Boards.Count);

            yield break;
        }


    }
}
