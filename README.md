# DUNGEON LIEGE

Grab your weapons, a couple of friends and embark on fantastic and dangerous
adventures! Stock up on items, learn incredible actions and boost your abilities
with impressive perks using an open and customizable RPG system! 

Dungeon Liege is an action packed dungeon crawler that can be played solo, 
but is best played with a few friends against yet another friend who plays 
the part of the DUNGEON LIEGE, either creating a great story ahead of time,
or changing up the odds and story as time goes on!

The game is built around the idea of a Dungeon, but this doesn't mean it is 
only a maze or a dungeon; cities, temples, ruins, jungles, vast plains and more
can also be their own dungeon, their own labyrinth. This game is an open module
based adventure, where you can easily tweak the campaign shipped with the game,
build your own modules, share modules with your friends and even download 
campaigns from within the game itself. 

You can also create new animations, actions, item and character skins, items, 
monsters, world objects, dungeon tiles and perks, share them with your friends,
or download them from the game!

This game is meant to harken back to the great dungeon divers and customizable 
cRPG's of yore, but to offer them with the modern conveniences and ease of
creation that games today have.

I hope that this game leads you on toward endless adventures and happiness as 
you become the ultimate DUNGEON LIEGE.
